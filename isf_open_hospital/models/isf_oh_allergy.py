from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv

class isf_oh_allergy(osv.Model):
    _name = 'isf.oh.allergy'
    
    _columns = {
        'name' : fields.char('Allergy'),
        'note' : fields.char('Note'),
    }