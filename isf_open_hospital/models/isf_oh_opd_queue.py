from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv
import time
from datetime import datetime
import openerp

_debug=False
_logger = logging.getLogger(__name__)

class isf_oh_opd_queue(osv.Model):
	_name = 'isf.oh.opd.queue'

	_columns = {
		'year' : fields.char('Year',size=4),
		'month' : fields.char('Month',size=2),
		'day' : fields.char('Day',size=2),
		'value' : fields.integer('Value'),
	}

	def get_next(self, cr, uid, year, month, day):
		val_ids = self.search(cr, uid, [('year','=',year),('day','=',day),('month','=',month)])
		if len(val_ids) > 0:
			val = self.browse(cr, uid, val_ids)[0]
			new_val = val.value
			rec_val = new_val + 1
			self.write(cr, uid, [val.id],{'value' : rec_val})
			return new_val
		else:
			self.create(cr, uid, {
				'year' : year,
				'month' : month,
				'day' : day,
				'value' : 1,
				})
		return 1


