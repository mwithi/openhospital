from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv
import time
from datetime import datetime
import openerp

_debug=False
_logger = logging.getLogger(__name__)

class isf_oh_encounter_item(osv.Model):
	_name = 'isf.oh.encounter.item'

	_columns = {
		'encounter_id' : fields.many2one('isf.oh.encounter','Encounter',select=True),
		#'name' : fields.char('Name',size=64),
		'model' : fields.char('Model',size=64),
		'record' : fields.integer('Record'),
	}

class isf_oh_encounter(osv.Model):
	_name = 'isf.oh.encounter'

	_rec_name = 'source'

	_columns = {
		'patient' : fields.many2one('isf.oh.patient','Patient',select=True),
		'source' : fields.char('Encounter',size=64),
		#'invoice_id' : fields.many2one('account.invoice','Invoice'),
		'state' : fields.selection([('open','Open'),('close','Close')]),
		'item_list' : fields.one2many('isf.oh.encounter.item','encounter_id','Items'),
	}

	_defaults = {
		'state' : 'open',
	}
	
	def find_open_encounter_by_patient(self, cr, uid, patient):
		enc_ids = self.search(cr, uid, [('state','=','open'),('patient','=',patient)])
		if len(enc_ids) > 1:
			raise openerp.exceptions.Warning(_('Found more encounters open for this patient, please manage encounters first'))
		for enc in self.browse(cr, uid, enc_ids):
			return enc.id
		return None

	def find_open_encounter_by_source(self, cr, uid, source):
		enc_ids = self.search(cr, uid, [('state','=','open'),('source','=',source)])
		if len(enc_ids) > 1:
			raise openerp.exceptions.Warning(_('Found more encounters with same source, please manage encounters first'))
		for enc in self.browse(cr, uid, enc_ids):
			return enc.id
		return None

	def add_event_to_encounter(self, cr, uid, encounter_id, model, record):
		item_o = self.pool.get('isf.oh.encounter.item')
		item_o.create(cr, uid, {
			'encounter_id' : encounter_id,
			'model' : model,
			'record' : record,
			})
		
	def reopen(self, cr, uid, ids):
		if ids:
			enc_obj = self.pool.get('isf.oh.encounter')
			enc_obj.write(cr, uid, [ids], {'state' : 'open'})
		
		else:	
			self.write(cr, uid, {'state' : 'open'})
	
	def close(self, cr, uid, ids):
		if ids:
			enc_obj = self.pool.get('isf.oh.encounter')
			enc_obj.write(cr, uid, [ids], {'state' : 'close'})
		
		else:	
			self.write(cr, uid, {'state' : 'close'})
