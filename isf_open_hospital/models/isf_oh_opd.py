from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv
import time
import datetime
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
import openerp

_logger = logging.getLogger(__name__)
_debug=False

class isf_oh_opd_investigation_other(osv.Model):
    _name = 'isf.oh.opd.investigation.other'
    
    _columns = {
        'name' : fields.char('Name'),
    }
    
class isf_oh_opd_drug_line(osv.Model):
    _name = 'isf.oh.opd.drug.line'
    
    _columns = {
        #'drug_id' : fields.many2one('product.product','Drugs',domain=[('pharmacy','=',True)]),
        'drug_id' : fields.many2one('product.product','Drugs'),
        'qty' : fields.integer('Qty'),
    }
    
class isf_oh_opd_other_line(osv.Model):
    _name = 'isf.oh.opd.other.line'
    
    _columns = {
        'text' : fields.char('Text'),
        'qty' : fields.integer('Qty'),
    }

class isf_oh_opd(osv.Model):
    _description = 'ISF OH OPD Module'
    _name = 'isf.oh.opd'
    _rec_name = 'opd_sequence'
    
    def _get_attendance_type(self, cr, iud, context=None):
        return (
            ('rea', 'Re-attendance'),
            ('new', 'New attendance'),
            #('fu', 'Follow-Up'),
        )
    
    def _get_discharge_type(self, cr, iud, context=None):
        return (
            ('d', 'Discharged'),
            ('de', 'Dead')
        )
        
    def _get_action_type(self, cr, iud, context=None):
        return (
            ('ht', 'Home Treatment'),
            ('fu', 'Follow Up (In Days)'),
            ('ad', 'Admitted'),
            ('ra', 'Referred'),
            ('ex', 'Escaped'),
        )
    
    _columns = {
        'state' : fields.selection([('new','New'),('visiting','Visiting'),('done','Done')],'State',readonly=True),
        'attendance_type' : fields.selection(_get_attendance_type,'Attendance Type'),
        'opd_sequence' : fields.char('OPD No.',size=16),
        'patient_id' : fields.many2one('isf.oh.patient','Patient',required=True),
        'patient_name' : fields.related('patient_id','patient_name',type='char',relation='isf.oh.patient',readonly=True,store=True,string='Name'),
        'patient_yy' : fields.integer('Years', required=False),
        'patient_mm' : fields.integer('Months', required=False),
        'patient_dd' : fields.integer('Days', required=False),
        'patient_sex' : fields.related('patient_id','sex',type='selection',relation='isf.oh.patient',readonly=True,store=False,string='Sex',selection=[('m','Male'),('f','Female')]),
        'attendance_date' : fields.datetime('Attendance Date', required=True),
        'referral_from' : fields.boolean('Referral From'),
        'referral_to' : fields.boolean('Referral To'),
        'diagnosis_ids' : fields.many2many('isf.oh.disease',string='Diagnosis'),
        'visit_ids' : fields.one2many('isf.oh.opd.visit','opd_id','Visits'),
        'present_compliant' : fields.text('Presenting Compliant(s)'),
        'observation' : fields.text('Observation and Findings'),
        'summary' : fields.text('Summary and Interpretation of findings'),
        'action_taken' : fields.selection(_get_action_type,'Action Taken'),
        'follow_up_days' : fields.integer('Days'),
        #'previous_opd_id' : fields.many2one('isf.oh.opd', 'Follow'),
        #Investigation
        'exam_ids' : fields.one2many('isf.oh.exam.request','opd_id','Exams'),
        'other_ids' : fields.many2many('isf.oh.opd.investigation.other','isf_oh_opd_other_rel','opd_id','other_id','Others'),
        #'drugs_ids' : fields.many2many('product.product','isf_oh_opd_drugs_rel','opd_id','product_id','Drugs',domain=[('pharmacy','=',True)]),
        'drugs_ids' : fields.many2many('isf.oh.opd.drug.line','isf_oh_opd_drug_line_rel','opd_id','line_id','Drugs'),
        'order_ids' : fields.many2many('isf.oh.opd.investigation.other','isf_oh_opd_order_rel','opd_id','order_id','Others'),
        'other_drugs_ids' : fields.many2many('isf.oh.opd.other.line','isf_oh_opd_other_line_drugs_rel','opd_id','other_drugs_id','Other Drugs'),
        'doctor_id' : fields.many2one('hr.employee','Doctor'),
        'req_exam_id' : fields.many2one('isf.oh.exam.request','Exam Request'),
        'job_id' : fields.many2one('hr.job','Job'),
        'doc_ro' : fields.boolean('Doc RO'),
        'clinic_id' : fields.many2one('isf.oh.clinic','Clinic'),
        'encounter_id' : fields.many2one('isf.oh.encounter','Encounter'),
    }
    
    _defaults = {
        'attendance_date' : lambda *a: time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
        'state' : 'new',
        'attendance_type' : 'new',
        'doc_ro' : False,
    }
    
    def check_datetime_not_in_future(self, cr, uid, ids, context=None):
        isf_oh_utils = self.pool.get('isf.open.hospital.utils')
        check = False
        for obj in self.browse(cr, uid, ids):
            check = isf_oh_utils.check_datetime_not_in_future(cr, uid, obj.attendance_date)
            
        return check
    
    _constraints = [
        (check_datetime_not_in_future, 'Date in the future not allowed.', ['attendance_date'])
    ]
    
    def get_default_job_type_doctor(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','job_type_doctor')
    
    def _get_opd_start_number(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','opd_start_number')
        
    def _get_account_enabled(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','account_enabled')
    
    def default_get(self, cr, uid, fields, context=None):
        data = super(isf_oh_opd, self).default_get(cr, uid, fields, context=context)
        job_id = self.get_default_job_type_doctor(cr, uid)
        data['job_id'] = job_id
        
        # If the user is not admin
        if uid != 1:
            hr_o = self.pool.get('hr.employee')
            hr_ids = hr_o.search(cr, uid, [('user_id','=',uid),('job_id','=',job_id)])
            if len(hr_ids) > 0:
                emp = hr_o.browse(cr, uid, hr_ids)[0]
                data['doctor_id'] = emp.id
                data['doc_ro'] = True
        
        patient_id = context.get('patient_id')
        if patient_id:
            data['patient_id'] = patient_id
        
        opd_seq = context.get('opd_sequence')
        if opd_seq:
            data['opd_sequence'] = opd_seq    

        enc_id = context.get('encounter_id')
        if enc_id:
            data['encounter_id'] = enc_id
            
        if _debug:
            _logger.debug('patient_id : %s', patient_id)
            _logger.debug('opd_seq : %s', opd_seq)
            _logger.debug('enc_id : %s', enc_id)        

        return data
    
    def name_get(self, cr, uid, ids, context=None):
        res = []    
        if ids:
            data = self.browse(cr, uid, ids)[0]
            name = 'OPD_' + str(data.id)
            res.append((ids[0], name))
        return res
    
    def create(self, cr, uid, vals, context=None):
        
        obj_sequence = self.pool.get('ir.sequence')
        obj_period = self.pool.get('account.period')
        seq_ids = obj_sequence.search(cr, uid, [('name','=','OPD')])
        date = datetime.datetime.now()
        period_ids = obj_period.find(cr, uid, date, context=context)
        period = obj_period.browse(cr, uid, period_ids)[0]
        ctx = context.copy()
        ctx.update({
            'fiscalyear_id': period.fiscalyear_id.id,
        })
        new_seq = obj_sequence.next_by_id(cr, uid, seq_ids[0],ctx)
    
        vals['opd_sequence'] = new_seq
        patient_id = ctx['patient_id']
        
        opd_id = super(isf_oh_opd, self).create(cr, uid, vals, context=context)
        
        # Create Encounter
        enc_o = self.pool.get('isf.oh.encounter')
        enc_id = enc_o.create(cr, uid, {'source' : new_seq, 'patient' : patient_id})
        enc_o.add_event_to_encounter(cr, uid, enc_id, 'isf.oh.opd', opd_id)
        
        vals['encounter_id'] = enc_id
        super(isf_oh_opd, self).write(cr, uid, [opd_id], vals, context=context)
        
        return opd_id
    
    def onchange_patient(self, cr, uid, ids, patient_id, context=None):
        result = {'value' : {}}
        
        pat_o = self.pool.get('isf.oh.patient')
        pat_ids = pat_o.search(cr, uid, [('id','=',patient_id)])
        patient = pat_o.browse(cr, uid, pat_ids)[0]
        
        result['value'].update({
            'patient_name' : patient.patient_name,
            'patient_yy' : patient.age_yy_view,
            'patient_mm' : patient.age_mm_view,
            'patient_dd' : patient.age_dd_view,
            'patient_sex' : patient.sex,
        })
        
        return result
        
    def get_opd_shop_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','opd_shop_id')
        
    def save_patient(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state':'new'}, context=context)
        return True
        
    def register_payment(self, cr, uid, ids, context=None):
        return self.manage_payment(cr, uid, ids, context=context)
    
    def next_and_register_payment(self, cr, uid, ids, context=None):
        self.save_patient(cr, uid, ids, context=context)
        return self.register_payment(cr, uid, ids, context=context)
            
    def manage_payment(self, cr, uid, ids, context=None):
        this = self.browse(cr, uid, ids)[0]
        shop_id = self.get_opd_shop_id(cr, uid)
        
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'isf_pos', 'view_isf_pos_form')
        
        desc = "Visit OPD number "+str(this.opd_number)
        return {
            'name':_("Register Payment"),
            'view_mode': 'form',
            'view_id': view_id,
            'view_type': 'form',
            'res_model': 'isf.pos',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'domain': '[]',
            'context': {
                'shop_id' : shop_id,
                'description' : desc,
                'patient_id' : this.patient_id.id,
                'opd_id' : this.id,
                'close_after_process': False,
            }
        }
        
    def _manage_investigation(self, cr, uid, ids, context=None):
        wiz = self.browse(cr, uid, ids)[0]
        req_o = self.pool.get('isf.oh.exam.request')
        req_line_o = self.pool.get('isf.oh.exam.request.line')
        
        req_id = False
        if wiz.exam_ids:
            req_id = req_o.create(cr, uid, {
                'patient_id' : wiz.patient_id.id,
                'date' : wiz.attendance_date,
                'ref' : wiz.opd_sequence,
                'state' : 'pending',
            }, context=context) 
        
        req_line_ids = []
        for exam in wiz.exam_ids:
            req_line_id = req_line_o.create(cr, uid, {
                'exam_id' : exam.id,
                'statement_id' : req_id,
            }, context=None)
        
        
        self.write(cr, uid, ids, {'req_exam_id' : req_id}, context=None)
        
            
    
    def reopen_opd(self, cr, uid, ids, context=None):
        wiz = self.browse(cr, uid, ids)[0]
        req_o = self.pool.get('isf.oh.exam.request')
        if wiz.req_exam_id:
            req_o.unlink(cr, uid, wiz.req_exam_id.id, context=context)
            self.write(cr, uid, ids, {'req_exam_id' : False}, context=None)
        self.write(cr, uid, ids, {'state':'visiting'},context=context)
        
        enc_o = self.pool.get('isf.oh.encounter')
        enc_o.reopen(cr, uid, wiz.encounter_id.id)
        
        return True
            
    def confirm(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state':'visiting'},context=context)
        self._manage_accounting(cr, uid, ids, context=context)
        return True
    
    def done(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        if data.diagnosis_ids is None or not data.diagnosis_ids:
            raise openerp.exceptions.Warning(_('You have to choose at least one Diagnosis in order to end the case'))
            return False
        
        if data.action_taken is None or not data.action_taken:
            raise openerp.exceptions.Warning(_('You have to choose at least one Action Taken in order to end the case'))
            return False
                
        self._manage_investigation(cr, uid, ids, context=context)
        self.write(cr, uid, ids, {'state':'done'},context=context)
        
        enc_obj = self.pool.get('isf.oh.encounter')
        enc_obj.close(cr, uid, data.encounter_id.id)
        
        return True

    def _get_journal_id_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','journal_id')

    def _manage_accounting(self, cr, uid, ids, context=None):
        account_enabled = self._get_account_enabled(cr, uid)
        if _debug:
            _logger.debug('OPD : Manage Accounting : %s',account_enabled)
        if account_enabled:
            if _debug:
                _logger.debug('OPD : Invoicing')
            self.action_invoice_create(cr,uid,ids,context=context)

    def _get_company_currency_id(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = users.company_id.id
        currency_id = users.company_id.currency_id
        
        return currency_id.id

    def _get_company_id(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        return users.company_id.id

    def _get_income_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','income_account_id')

    def _get_opd_product_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','opd_product_id')

    def _get_opd_aa_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','opd_aa_id')

    def action_invoice_create(self, cr, uid, ids, context=None):
        opd = self.browse(cr, uid, ids)[0]
        journal_id = self._get_journal_id_id(cr, uid)
        rcv_acc_id = opd.patient_id.partner_id.property_account_receivable.id
        currency_id = self._get_company_currency_id(cr, uid, context=context)
        company_id = self._get_company_id(cr, uid, context=context)
        acc_id = self._get_income_account_id( cr, uid)
        opd_product_id = self._get_opd_product_id(cr, uid)
        aa_id = self._get_opd_aa_id(cr, uid)

        product_obj = self.pool.get('product.product')
        product_ids = product_obj.search(cr, uid, [('id','=',opd_product_id)])
        product = product_obj.browse(cr, uid, product_ids)[0]

        inv_obj = self.pool.get('account.invoice')
        inv_line_obj = self.pool.get('account.invoice.line')

        inv_line = {
            'name': 'OPD '+opd.clinic_id.name,
            'account_id': acc_id,
            'price_unit': product.list_price,
            'quantity': 1.0,
            'product_id': opd_product_id,
            'uos_id': False,
            'invoice_line_tax_id': False,
            'account_analytic_id': aa_id,
        }
        inv_line_id = inv_line_obj.create(cr, uid, inv_line, context=context)
        inv_lines = []
        inv_lines.append(inv_line_id)
        inv_data = {
            'name': opd.opd_sequence,
            'reference': opd.opd_sequence,
            'account_id': rcv_acc_id,
            'type': 'out_invoice',
            'partner_id': opd.patient_id.partner_id.id,
            'currency_id': currency_id,
            'journal_id': journal_id,
            'invoice_line': [(6, 0, inv_lines)],
            'origin': opd.opd_sequence,
            'fiscal_position': False,
            'payment_term': False,
            'company_id': company_id,
        }
        inv_id = inv_obj.create(cr, uid, inv_data, context=context)

        enc_o = self.pool.get('isf.oh.encounter')
        enc_o.add_event_to_encounter(cr, uid, opd.encounter_id.id, 'account.invoice', inv_id)
        
        return inv_id

    def open_exam_request(self, cr, uid, ids, context=None):
        this = self.browse(cr, uid, ids)[0]
        
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'isf_open_hospital', 'isf_oh_exam_request_form')
        
        enc_o = self.pool.get('isf.oh.encounter')
        enc_id = enc_o.find_open_encounter_by_source(cr, uid, this.opd_sequence)
        return {
            'name':_("Exam Request"),
            'view_mode': 'form',
            'view_id': view_id,
            'view_type': 'form',
            'res_model': 'isf.oh.exam.request',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'domain': '[]',
            'context': {
                'source' : 'opd',
                'ref' : this.opd_sequence,
                'patient_id' : this.patient_id.id,
                'close_after_process': False,
                'opd_id' : this.id,
                'encounter_id' : enc_id,
            }
        }  

isf_oh_opd()