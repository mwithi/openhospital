from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv
import datetime
import openerp
import time

_debug=False
_logger = logging.getLogger(__name__)
        

##############################################################################################################################################################################
################################################################ LAB  HISTORY #########################################################################################
##############################################################################################################################################################################



class isf_oh_exam_history(osv.Model):
    _name = 'isf.oh.lab.history'
     
    _columns = {
        'date' : fields.datetime('Date'),
        'patient_id' : fields.many2one('isf.oh.patient','Patient'),
        'exam_id' : fields.many2one('isf.oh.exam','Exam'),   
        'test' : fields.char('Test'), 
        'normal_range' : fields.related('exam_id','normal_range',type='char',rel='isf.oh.exam',string='Normal Range'),
        'result' : fields.char('Result'),    
        'note' : fields.text('Note'),
        'request_id' : fields.many2one('isf.oh.exam.request','Request'),
        'technician_id' : fields.many2one('hr.employee','Technician'),
    }
    

##############################################################################################################################################################################
##########################################################################  EXAM REQUEST #####################################################################################
##############################################################################################################################################################################

class isf_oh_exam_request_line_select(osv.TransientModel):
    _name = 'isf.oh.exam.request.line.select'
    
    _columns = {
        'exam_ids': fields.many2many('isf.oh.exam', 'isf_oh_exam_select_request_rel', 'request_id', 'product_id', 'Exams'),
    }
    
    def import_exam(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        line_ids = data['exam_ids']
        active_id = context.get('active_id')
        line_obj = self.pool.get('isf.oh.exam.request.line')
        
        if _debug:
            _logger.debug('Active ID : %s', active_id)
        
        for exam in line_ids:
            line_id = line_obj.create(cr, uid, {
                'exam_id' : exam.id,
                'statement_id' : active_id,
            }, context=context)
                
        return True
    
class isf_oh_exam_request_line(osv.Model):
    _name = 'isf.oh.exam.request.line'
    
    _columns = {
        'state' : fields.selection([('pending','Pending'),('done','Done'),('cancel','Cancelled')],'Status'),
        'exam_id': fields.many2one('isf.oh.exam', 'Exams'),
        'exam_code' : fields.related('exam_id','code',type='char',rel='isf.oh.exam',string='Code'),
        'exam_normal_range' : fields.related('exam_id','normal_range',type='char',rel='isf.oh.exam',string='Normal Range'),
        'exam_uom' : fields.related('exam_id','uom_id','name',type='char',rel='isf.oh.uom',string='UoM'),
        'technician_id' : fields.many2one('hr.employee','Technician'),
        'statement_id' : fields.many2one('isf.oh.exam.request','Request',select=True),
        'result' : fields.char('Result'),
    }
    
    _defaults = {
        'state' : 'pending',
    }
    
class isf_oh_exam_request(osv.Model):
    _name = 'isf.oh.exam.request'
    _rec_name = 'request_id'
    
    def _pre_load_exams(self, cr, uid, context=None):
        if _debug:
            _logger.debug('CONTEXT : %s',context)
        statement_id = context.get('active_id')
        source = context.get('source')
        exam_o = self.pool.get('isf.oh.exam')
        exam_ids = exam_o.search(cr, uid, [])
        
        ref = None
        patient_id = None
        if source == 'admission':
            statement_id = None
            ref = context.get('ref')
            patient_id = context.get('patient_id')
        
        for exam in exam_o.browse(cr, uid, exam_ids):
            if _debug:
                _logger.debug('Exam : %s | Active Id : %s',exam.description,statement_id)
            line_o = self.pool.get('isf.oh.exam.request.line')
            line_o.create(cr, uid, {
                'exam_id' : exam.id,
                'statement_id' : statement_id,
            })
        
    def default_get(self, cr, uid, fields, context=None):
        res = super(isf_oh_exam_request, self).default_get(cr, uid, fields, context=context)
        #self._pre_load_exams(cr, uid, context=context)
        
        source = context.get('source')
        if source == 'admission':
            res['ref'] = context.get('ref')
            res['patient_id'] = context.get('patient_id')
            res['source'] = source
            res['admission_id'] = context.get('admission_id')
            
        if source == 'opd':
            res['ref'] = context.get('ref')
            res['patient_id'] = context.get('patient_id')
            res['source'] = source
            res['opd_id'] = context.get('opd_id')

        enc_id = context.get('encounter_id')
        if _debug:
            _logger.debug('Encounter : %s',enc_id)
        
        return res
    
    _columns = {
        'source' : fields.char('Socurce'),
        'state' : fields.selection([('draft','Draft'),('new','New'),('pending','Pending'),('done','Close')],'State',readonly=True),
        'request_id' : fields.char('Request Id',size=32),
        'date' : fields.date('Date'),
        'patient_id' : fields.many2one('isf.oh.patient','Patient Id'),
        'note' : fields.text('Note'),
        'exams_id': fields.one2many('isf.oh.exam.request.line','statement_id', 'Exams'),
        'ref' : fields.char('Reference'),
        'invoice_id' : fields.many2one('account.invoice','Invoice Id'),
        'opd_id' : fields.many2one('isf.oh.opd','Opd',select=True),
        'admission_id' : fields.many2one('isf.oh.admission','Admission',select=True),
        'encounter_id' : fields.many2one('isf.oh.encounter','Encounter'),
    }
    
    _defaults = {
        'state' : 'draft',
        'date' : fields.date.context_today,
        'source' : 'self',
    }
    
    
    def create(self, cr, uid, vals, context=None):
        obj_sequence = self.pool.get('ir.sequence')
        obj_period = self.pool.get('account.period')
        enc_o = self.pool.get('isf.oh.encounter')
        seq_ids = obj_sequence.search(cr, uid, [('name','=','ExamRequest')])
        date = datetime.datetime.now()
        period_ids = obj_period.find(cr, uid, date, context=context)
        period = obj_period.browse(cr, uid, period_ids)[0]
        ctx = context.copy()
        ctx.update({
            'fiscalyear_id': period.fiscalyear_id.id,
        })
        new_seq = obj_sequence.next_by_id(cr, uid, seq_ids,ctx)
        
        if _debug:
            _logger.debug('CTX : %s', context)
        
        enc_id = context.get('encounter_id')
        vals['request_id'] = new_seq
        vals['encounter_id'] = enc_id
#         if enc_id:
#             vals['encounter_id'] = enc_id
#             enc_o.add_event_to_encounter(cr, uid, enc_id, new_seq)
#         else:
#             enc_id = enc_o.create(cr, uid, {'source' : new_seq})
#             vals['encounter_id'] = enc_id
#             enc_o.add_event_to_encounter(cr, uid, enc_id, new_seq)
        
        res_id = super(isf_oh_exam_request, self).create(cr, uid, vals, context=context)  
        enc_o.add_event_to_encounter(cr, uid, enc_id, 'isf.oh.exam.request', res_id)
    
        return res_id
       
    def set_to_new(self, cr, uid, ids, context=None):
        self._delete_invoice(cr, uid, ids, context=context)
        self.write(cr, uid, ids, {'state':'new'}, context=context)
        return True
        
    def confirm(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state':'pending'}, context=context)
        return True

    def request(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state':'pending'}, context=context)
        self._manage_accounting(cr, uid, ids, context=context)
        return True
        
    def register_result(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        mod_obj = self.pool.get('ir.model.data')
        res = mod_obj.get_object_reference(cr, uid, 'isf_open_hospital', 'isf_oh_exam_result_wizard_form')   
        ctx = context.copy()
        ctx.update({
            'request_id' : data.id,
        }) 
        result = {
            'name': 'Patient',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res and res[1] or False],
            'res_model': 'isf.oh.exam.result.wizard',
            'context': ctx,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
        }   
        return result
        
    def close(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        
        view_msg_box = False
        for exam in data.exams_id:
            if exam.state == 'pending':
                raise openerp.exceptions.Warning(_('Some exams are pending. Check the request.'))
                return False
                
        if not view_msg_box:
            self.write(cr, uid, ids, {'state':'done'},context=context)
            
    def set_to_pending(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state' : 'pending'},context=context)

    def _get_account_enabled(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','account_enabled')

    def _get_journal_id_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','journal_id')

    def _manage_accounting(self, cr, uid, ids, context=None):
        account_enabled = self._get_account_enabled(cr, uid)
        if account_enabled:
            self.action_invoice_create(cr,uid,ids,context=context)

    def _get_company_currency_id(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = users.company_id.id
        currency_id = users.company_id.currency_id
        
        return currency_id.id

    def _get_company_id(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        return users.company_id.id

    def _get_income_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','income_account_id')

    def _get_lab_aa_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','lab_aa_id')

    def action_invoice_create(self, cr, uid, ids, context=None):
        request = self.browse(cr, uid, ids)[0]
        journal_id = self._get_journal_id_id(cr, uid)
        rcv_acc_id = request.patient_id.partner_id.property_account_receivable.id
        currency_id = self._get_company_currency_id(cr, uid, context=context)
        company_id = self._get_company_id(cr, uid, context=context)
        acc_id = self._get_income_account_id( cr, uid)
        aa_id = self._get_lab_aa_id(cr, uid)
        
#         create_invoice = True
#         inv_id = False
#         if request.encounter_id.state == 'open':
#             if request.encounter_id.invoice_id:
#                 inv_id = request.encounter_id.invoice_id.id
#                 create_invoice = False

        inv_obj = self.pool.get('account.invoice')
        inv_line_obj = self.pool.get('account.invoice.line')

        inv_lines = []
        for exam in request.exams_id:

            inv_line = {
                'name': exam.exam_id.name,
                'account_id': acc_id,
                'price_unit': exam.exam_id.product_id.list_price,
                'quantity': 1.0,
                'product_id': exam.exam_id.product_id.id,
                'uos_id': False,
                'invoice_line_tax_id': False,
                'account_analytic_id': aa_id,
                'invoice_id' : False,
            }
            inv_line_id = inv_line_obj.create(cr, uid, inv_line, context=context)
            inv_lines.append(inv_line_id)

        inv_data = {
            'name': request.request_id,
            'reference': request.request_id,
            'account_id': rcv_acc_id,
            'type': 'out_invoice',
            'partner_id': request.patient_id.partner_id.id,
            'currency_id': currency_id,
            'journal_id': journal_id,
            'invoice_line': [(6, 0, inv_lines)],
            'origin': request.request_id,
            'fiscal_position': False,
            'payment_term': False,
            'company_id': company_id,
        }

#         if create_invoice:
        inv_id = inv_obj.create(cr, uid, inv_data, context=context)
        self.write(cr, uid, ids, {'invoice_id':inv_id},context=context)
        
        enc_o = self.pool.get('isf.oh.encounter')
        #enc_o.write(cr,uid,request.encounter_id.id,{'invoice_id':inv_id})    
        enc_o.add_event_to_encounter(cr, uid, request.encounter_id.id, 'account.invoice', inv_id)
        
        self.write(cr, uid, ids, {'invoice_id':inv_id},context=context)
        
        return inv_id

    def _delete_invoice(self, cr, uid, ids, context=None):
        account_enabled = self._get_account_enabled(cr, uid)
        if account_enabled:
            data = self.browse(cr, uid, ids)[0]
            if data.invoice_id:
                inv_obj = self.pool.get('account.invoice')
                inv_obj.action_cancel(cr, uid, [data.invoice_id.id], context=context)