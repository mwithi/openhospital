from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv
import time
from datetime import datetime
import openerp
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)
_debug=False


class isf_oh_admission_type(osv.Model):
    _name = 'isf.oh.admission.type'
    
    _columns = {
        'name' : fields.char('Admission Type', required=True),
    }

class isf_oh_discharge_type(osv.Model):
    _name = 'isf.oh.discharge.type'
    
    _columns = {
        'name' : fields.char('Discharge Type', required=True),
    }
    
class isf_oh_delivery_type(osv.Model):
    _name = 'isf.oh.delivery.type'
    
    _columns = {
        'name' : fields.char('Delivery Type', required=True),
    }
    
class isf_oh_delivery_result_type(osv.Model):
    _name = 'isf.oh.delivery.result.type'
    
    _columns = {
        'name' : fields.char('Delivery Result Type', required=True),
    }


class isf_oh_admission(osv.Model):
    _name = 'isf.oh.admission'

    def _open_bill_on_admission(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','open_bill_on_admission')

    def _get_company_currency_id(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = users.company_id.id
        currency_id = users.company_id.currency_id
        
        return currency_id.id

    def _get_company_id(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        return users.company_id.id

    def _get_income_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','income_account_id')

    def _get_journal_id_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','journal_id')

    def _get_adm_product_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','adm_product_id')

    def _get_account_enabled(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','account_enabled')
    
    def _get_lenght_of_stay(self, cr, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}
            
        res={}
        
        for i in ids:
            _logger.info(i)
            
            d = self.browse(cr, uid, i)
            if d.discharge_date:
                dis_date = datetime.strptime(d.discharge_date, DEFAULT_SERVER_DATETIME_FORMAT)
                adm_date = datetime.strptime(d.admission_date, DEFAULT_SERVER_DATETIME_FORMAT)
                lof = (dis_date - adm_date).days
                if lof == 0:
                    res[d.id] = 1
                else:
                    res[d.id] = lof
            else:
                res[d.id] = 0

        return res
        
    
    _rec_name = 'admission_sequence'
        
    _columns = {
        'state' : fields.selection([('new','New'),('admitted','Admitted'),('discharged','Discharged')],required=True),
        'admission_sequence' : fields.char('Admission No.',size=16),
        'patient_id' : fields.many2one('isf.oh.patient','Patient', required=True),
        'patient_name' : fields.related('patient_id','patient_name',type='char',relation='isf.oh.patient',readonly=True,store=True,string='Name'),
        'patient_sex' : fields.related('patient_id','sex',type='selection',relation='isf.oh.patient',readonly=True,store=False,string='Sex',selection=[('m','Male'),('f','Female')]),
        'patient_yy' : fields.integer('Years', required=False),
        'patient_mm' : fields.integer('Months', required=False),
        'patient_dd' : fields.integer('Days', required=False),
        'admission_date' : fields.datetime('Admission Date', required=True),
        'discharge_date' : fields.datetime('Discharge Date', required=False),
        'lenght_of_stay' : fields.function(_get_lenght_of_stay,type='integer',string='Lenght Of Stay'),
        'admission_type_ids' : fields.many2one('isf.oh.admission.type','Admission Type',required=True),
        'discharge_type_ids' : fields.many2one('isf.oh.discharge.type','Discharge Type',required=False),
        'ward' : fields.many2one('isf.oh.ward','Ward',required=True),
        'bed' : fields.many2one('isf.oh.bed','Bed No.',required=False),
        'diagnosis_in_id' : fields.many2one('isf.oh.disease','Diagnosis IN', required=True),
        'diagnosis_out_ids' : fields.many2many('isf.oh.disease',string="Diagnosis OUT",required=False),
        'note' : fields.text('Note', required=False),
        'treatment' : fields.text('Treatment on Admission', required=False),
        'monitoring_ids' : fields.one2many('isf.oh.monitoring','adm_id','Monitoring'),
        # Vital Sign on Admission
        'weight' : fields.float('Weight (Kg)'),
        'temperature' : fields.float('Temp.(C)'),
        'bp' : fields.float('BP (mmhg)'),
        'pr' : fields.float('PR (/m)'),
        'rr' : fields.float('RR (/m)'),
        'progress_ids' : fields.one2many('isf.oh.progress.note','adm_id','Progress Note'),
        # Operations
        'patient_operation_ids' : fields.one2many('isf.oh.patient.operation','admission_id','Operations'),
        'encounter_id' : fields.many2one('isf.oh.encounter','Encounter'),
    }
    
    _defaults = {
        'state' : 'new',
        'admission_date' : lambda *a: time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
    }
    
    def check_datetime_not_in_future_adm(self, cr, uid, ids, context=None):
        isf_oh_utils = self.pool.get('isf.open.hospital.utils')
        check = False
        for obj in self.browse(cr, uid, ids):
            check = isf_oh_utils.check_datetime_not_in_future(cr, uid, obj.admission_date)
            
        return check
    
    def check_datetime_not_in_future_dis(self, cr, uid, ids, context=None):
        isf_oh_utils = self.pool.get('isf.open.hospital.utils')
        check = False
        for obj in self.browse(cr, uid, ids):
            check = isf_oh_utils.check_datetime_not_in_future(cr, uid, obj.discharge_date)
            
        return check
    
    def check_discharge_not_before_admission(self, cr, uid, ids, context=None):
        check = False
        for obj in self.browse(cr, uid, ids):
            check = obj.admission_date < obj.discharge_date
            
        return check
    
    _constraints = [
        (check_datetime_not_in_future_adm, 'Admission date in the future not allowed.', ['admission_date']),
    ]
    
    def default_get(self, cr, uid, fields, context=None):
        data = super(isf_oh_admission, self).default_get(cr, uid, fields, context=context)
        
        patient_id = context.get('patient_id')
        if patient_id:
            data['patient_id'] = patient_id
        
        seq = context.get('admission_sequence')
        if seq:
            data['admission_sequence'] = seq
        
        enc_id = context.get('encounter_id')
        if enc_id:
            data['encounter_id'] = enc_id
            
        return data
    
    def create(self, cr, uid, vals, context=None):
        obj_sequence = self.pool.get('ir.sequence')
        obj_period = self.pool.get('account.period')
        seq_ids = obj_sequence.search(cr, uid, [('name','=','Admission')])
        date = datetime.now()
        period_ids = obj_period.find(cr, uid, date, context=context)
        period = obj_period.browse(cr, uid, period_ids)[0]
        ctx = context.copy()
        ctx.update({
            'fiscalyear_id': period.fiscalyear_id.id,
        })
        new_seq = obj_sequence.next_by_id(cr, uid, seq_ids[0],ctx)
 
        vals['admission_sequence'] = new_seq
        patient_id = ctx['patient_id']
        
        adm_id = super(isf_oh_admission, self).create(cr, uid, vals, context=context)
        
        # Create Encounter
        enc_o = self.pool.get('isf.oh.encounter')
        enc_id = enc_o.create(cr, uid, {'source' : new_seq, 'patient' : patient_id})
        enc_o.add_event_to_encounter(cr, uid, enc_id, 'isf.oh.admission', adm_id)
        
        vals['encounter_id'] = enc_id
        super(isf_oh_admission, self).write(cr, uid, [adm_id], vals, context=context)
        
        return adm_id
    
    def admit_patient(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        self.write(cr, uid, ids, {'state':'admitted'}, context=context)
        open_bill = self._open_bill_on_admission(cr, uid)
        account_enabled = self._get_account_enabled(cr, uid)
        if data.bed:
            bed_o = self.pool.get('isf.oh.bed')
            bed_o.write(cr, uid, data.bed.id, {'state' : 'occupied'}, context=context)
        if open_bill and account_enabled:
            self.create_invoice(cr, uid, ids, context=context)
        return True
    
    def discharge_patient(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        if data.discharge_date is None or not data.discharge_date:
            raise openerp.exceptions.Warning(_('You have to set discharge date in order to discharge a patient'))
            return False
        
        if not self.check_datetime_not_in_future_dis(cr, uid, ids, context):
            raise openerp.exceptions.Warning(_('Discharge date in the future not allowed'))
            return False
        
        if not self.check_discharge_not_before_admission(cr, uid, ids, context):
            raise openerp.exceptions.Warning(_('Discharge date must be after the date of the Admission'))
            return False
            
        if data.discharge_type_ids is None or not data.discharge_type_ids:
            raise openerp.exceptions.Warning(_('You have to set discharge type in order to discharge a patient'))
            return False
        
        if data.diagnosis_out_ids is None or not data.diagnosis_out_ids:
            raise openerp.exceptions.Warning(_('You have to choose at least one Diagnosis OUT in order to discharge a patient'))
            return False
        
        if data.bed:
            bed_o = self.pool.get('isf.oh.bed')
            bed_o.write(cr, uid, data.bed.id, {'state' : 'free'}, context=context)

        account_enabled = self._get_account_enabled(cr, uid)
        open_bill_on_admission = self._open_bill_on_admission(cr, uid)
        if account_enabled and not open_bill_on_admission:
            self.create_invoice(cr, uid, ids, context=context)
        
        self.write(cr, uid, ids, {'state':'discharged'}, context=context)
        
        enc_obj = self.pool.get('isf.oh.encounter')
        enc_obj.close(cr, uid, data.encounter_id.id)
        
        return True
        
    def onchange_patient(self, cr, uid, ids, patient_id, context=None):
        result = {'value' : {}}
        
        pat_o = self.pool.get('isf.oh.patient')
        pat_ids = pat_o.search(cr, uid, [('id','=',patient_id)])
        patient = pat_o.browse(cr, uid, pat_ids)[0]
        
        result['value'].update({
            'patient_name' : patient.patient_name,
            'patient_yy' : patient.age_yy_view,
            'patient_mm' : patient.age_mm_view,
            'patient_dd' : patient.age_dd_view,
            'patient_sex' : patient.sex,
        })
        
        return result
        
    def onchange_ward(self, cr, uid, ids, ward, context=None):
        if context is None:
            context = {}
        
        result = {'value':{}}
        bed_ids = []
        adm_ids = self.search(cr, uid, [('ward','=',ward)])
        for adm in self.browse(cr, uid, adm_ids):
            if adm.bed:
                bed_ids.append(adm.bed.id)
                
        if _debug:
            _logger.debug('BED IDS : %s', bed_ids)
            
        return result
        
    def request_exams(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        this = self.browse(cr, uid, ids)[0]
        
        
        request_o = self.pool.get('isf.oh.exam.request')
        #res_id = request_o.create(cr, uid, {
        #    'patient_id' : this.patient_id.id,
        #    'ref' : this.admission_sequence,
        #}, context=context)
        mod_obj = self.pool.get('ir.model.data')
        res = mod_obj.get_object_reference(cr, uid, 'isf_open_hospital', 'isf_oh_exam_request_form')
    
        enc_o = self.pool.get('isf.oh.encounter')
        enc_id = enc_o.find_open_encounter_by_source(cr, uid, this.admission_sequence)
        ctx = context.copy()
        ctx.update({
            'patient_id' : this.patient_id.id,
            'ref' : this.admission_sequence,
            'source' : 'admission',
            'admission_id' : this.id,
            'encounter_id' : enc_id,
        })
    
        result = {
            'name': 'New',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res and res[1] or False],
            'res_model': 'isf.oh.exam.request',
            'context': ctx,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            #'res_id' : res_id,
        }
        
        return result

    def create_invoice(self, cr, uid, ids, context=None):
        adm = self.browse(cr, uid, ids)[0]
        journal_id = self._get_journal_id_id(cr, uid)
        rcv_acc_id = adm.patient_id.partner_id.property_account_receivable.id
        currency_id = self._get_company_currency_id(cr, uid, context=context)
        company_id = self._get_company_id(cr, uid, context=context)
        acc_id = self._get_income_account_id( cr, uid)
        adm_product_id = self._get_adm_product_id(cr, uid)
        aa_id = False
        if adm.ward.aa_id:
            aa_id = adm.ward.aa_id.id

        product_obj = self.pool.get('product.product')
        product_ids = product_obj.search(cr, uid, [('id','=',adm_product_id)])
        product = product_obj.browse(cr, uid, product_ids)[0]

        inv_obj = self.pool.get('account.invoice')
        inv_line_obj = self.pool.get('account.invoice.line')

        inv_line = {
            'name': 'Admission '+adm.ward.name,
            'account_id': acc_id,
            'price_unit': product.list_price,
            'quantity': 1.0,
            'product_id': adm_product_id,
            'uos_id': False,
            'invoice_line_tax_id': False,
            'account_analytic_id': aa_id,
        }
        inv_line_id = inv_line_obj.create(cr, uid, inv_line, context=context)
        inv_lines = []
        inv_lines.append(inv_line_id)
        inv_data = {
            'name': adm.admission_sequence,
            'reference': adm.admission_sequence,
            'account_id': rcv_acc_id,
            'type': 'out_invoice',
            'partner_id': adm.patient_id.partner_id.id,
            'currency_id': currency_id,
            'journal_id': journal_id,
            'invoice_line': [(6, 0, inv_lines)],
            'origin': adm.admission_sequence,
            'fiscal_position': False,
            'payment_term': False,
            'company_id': company_id,
        }
        inv_id = inv_obj.create(cr, uid, inv_data, context=context)

        enc_o = self.pool.get('isf.oh.encounter')
        enc_o.add_event_to_encounter(cr, uid, adm.encounter_id.id, 'account.invoice', inv_id)
        
        return inv_id
    
    def reopen_admission(self, cr, uid, ids, context=None):
#         wiz = self.browse(cr, uid, ids)[0]
#         req_o = self.pool.get('isf.oh.exam.request')
#         if wiz.req_exam_id:
#             req_o.unlink(cr, uid, wiz.req_exam_id.id, context=context)
#             self.write(cr, uid, ids, {'req_exam_id' : False}, context=None)
        self.write(cr, uid, ids, {'state':'admitted'},context=context)
        return True
    
    def redraft_admission(self, cr, uid, ids, context=None):
        wiz = self.browse(cr, uid, ids)[0]
#         req_o = self.pool.get('isf.oh.exam.request')
#         if wiz.req_exam_id:
#             req_o.unlink(cr, uid, wiz.req_exam_id.id, context=context)
#             self.write(cr, uid, ids, {'req_exam_id' : False}, context=None)
        self.write(cr, uid, ids, {'state':'new'},context=context)
        
        enc_o = self.pool.get('isf.oh.encounter')
        enc_o.reopen(cr, uid, wiz.encounter_id.id)
        
        return True
                
        
class isf_oh_monitoring(osv.Model):
    _name = 'isf.oh.monitoring'
    
    _columns = {
        'datetime' : fields.datetime('Date/Time'),
        'temperature' : fields.float('Temp.'),
        'bp' : fields.float('BP/m'),
        'plus' : fields.float('Plus'),
        'rr' : fields.float('RR/m'),
        'oxygen' : fields.float('Oxygen'),
        'input' : fields.float('Input'),
        'output' : fields.float('Output'),
        'adm_id' : fields.many2one('isf.oh.admission',string="Admission Id",select=True),
    }
    
    _defaults = {
        'datetime' : lambda * a: time.strftime('%Y-%m-%d %H:%M:%S'),
    } 
    
class isf_oh_progress_note(osv.Model):
    _name = 'isf.oh.progress.note'
    
    _columns = {
        'datetime' : fields.datetime('Date/Time'),
        'note' : fields.text('Note'),
        'adm_id' : fields.many2one('isf.oh.admission',string="Admission Id",select=True),
    }