from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.tools.translate import _
from openerp.osv import fields, osv
from openerp import tools
from openerp import netsvc
import logging
import datetime
import openerp
from lxml import etree

_logger = logging.getLogger(__name__)
_debug=False

#####################################################################################################################################################################
########################################################################### ISF POS #################################################################################
#####################################################################################################################################################################

class isf_pos(osv.Model):
    _name = 'isf.pos'
    _inherit = 'isf.pos'
    
    _columns = {
        #'partner_id' : fields.many2one('res.partner','Customer'),
        'patient_id': fields.many2one('isf.oh.patient','Patient'),
    }
    
    def onchange_patient(self, cr, uid, ids, patient_id, context=None):
        if _debug:
            _logger.debug('==> onchange_patient_id(%s)', patient_id)
        
        result = {'value' : {}}
        
        patient_obj = self.pool.get('isf.oh.patient')
        patient = patient_obj.browse(cr, uid, patient_id)
        if patient:
            result['value'].update({
                'partner_id' : patient.partner_id.id,
            })
        
        return result
