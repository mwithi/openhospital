from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv

_debug=False
_logger = logging.getLogger(__name__)

class isf_oh_disease_category(osv.Model):
    _name = 'isf.oh.disease.category'
    
    _columns = {
        'name' : fields.char('Category',size=64, required=True),
        'parent_id' : fields.many2one('isf.oh.disease.category','Parent Category',select=True, ondelete='cascade'),
        'child_id' : fields.one2many('isf.oh.disease.category','parent_id','Child Categories'),
    }
    
isf_oh_disease_category()

class isf_oh_disease(osv.Model):
    _name = 'isf.oh.disease'
    
    _rec_name = 'description'
       
    _columns = {
        'name' : fields.char('Code',size=16,required=True),
        'description' : fields.char('Description',size=128, required=True),
        'category' : fields.many2one('isf.oh.disease.category','Category',required=True),
        'min_age' : fields.integer('Minimum Age (months)'),
        'max_age' : fields.integer('Maximum Age (months)'),
        'gender' : fields.selection([('m','Male Disease'),('f','Female Disease'),('unisex','Unisex')],'Gender'),
        'diagnosis_opd' : fields.boolean('Available in OPD'),
        'diagnosis_in' : fields.boolean('Available in Admission'),
        'diagnosis_out' : fields.boolean('Available in Discharge'),
    }
    
    _defaults = {
        'min_age' : 0,
        'max_age' : 0,
        'gender' : 'unisex',
        'diagnosis_opd' : True,
        'diagnosis_in' : True,
        'diagnosis_out' : True,
    }
    
isf_oh_disease()

class isf_oh_disease_simplfied(osv.Model):
    _name = 'isf.oh.disease.simplified'

    _columns = {
        'name' : fields.char('Disease'),
    }

class isf_oh_disease_mapping(osv.Model):
    _name = 'isf.oh.disease.mapping'

    _columns = {
        'name' : fields.char('Description'),
        'disease_simple_id' : fields.many2one('isf.oh.disease.simplified','Disease Simplified'),
        'disease_ids' : fields.many2many('isf.oh.disease',string='Diseases'),
    }