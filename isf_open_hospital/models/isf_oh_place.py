from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv
import datetime
from datetime import date
import time
import pytz
import re

_logger = logging.getLogger(__name__)
_debug=False

class isf_oh_place(osv.Model):
    _name = 'isf.oh.place'
    _order = 'name'

    # Hargeisa+Police+Station/@9.561125,44.064455,17z
    
    _columns = {
        'parent_place_id' : fields.many2one('isf.oh.place','Parent Place'),
        'country_id' : fields.many2one('res.country','Country',required=True),
        'name' : fields.char('Name',required=True),
        'latitude' : fields.float('Latitude'),   
        'longitude' : fields.float('Longitude'),  
        'z' : fields.integer('Z'), 
    }
    
    def name_get(self, cr, uid, ids, context=None):
        res = []    
        
        if _debug:
            _logger.debug('IDS : %s',ids)
        
        for x in ids:
            data = self.browse(cr, uid, [x])[0]
            if data.country_id:
                name = data.country_id.name + " / " + data.name
            else:
                name = data.name
            res.append((x, name))
        return res

    def onchange_parent_place_id(self, cr, uid, ids, parent_place_id, context=None):
        if context is None:
            context = {}

        result = {'value': {}}
        parent = self.browse(cr, uid, [parent_place_id])[0]
        if parent:
            result['value'].update({
                'country_id' : parent.country_id.id,
                })

        return result
