from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv

class isf_oh_patient_operation(osv.Model):
    _name = 'isf.oh.patient.operation'
    
    _columns = {
        'state' : fields.selection([('new','New'),('done','Done')],'State',readonly=True),
        'admission_id' : fields.many2one('isf.oh.admission',string="Admission Id",select=True),
        'date' : fields.date('Date'),
        'operation_id' : fields.many2one('isf.oh.operation','Operation Type',domain=[('operations','=',True)]),
        'result' : fields.selection([('positive','Positive'),('negative','Negative'),('unknown','Unknown')],'Operation Result'),
        'ward_id' : fields.many2one('isf.oh.ward','Ward'),
         # Operations Check List
        'ot_informed' : fields.boolean('Informed Consent'),
        'ot_shaving' : fields.boolean('Shaving'),
        'ot_catheterizatiomn' : fields.boolean('Cathterization'),
        'ot_fasting' : fields.boolean('Fasting'),
        'ot_iv_line' : fields.boolean('IV Line'),
        #'ot_allergy' : fields.text('Any know allergy'),
        'ot_allergy_ids' : fields.related('admission_id','patient_id','allergy_ids',relation='isf.oh.allergy',type='many2many',string='Allergy'),
        'ot_anesthetic_risk' : fields.text('Anesthetic Risk'),
        'ot_anesthetic_method' : fields.many2one('isf.oh.anesthetic.method','Anesthetic Method'),
        'ot_anesthetic_induction' : fields.many2many('isf.oh.anesthetic.time.data','isf_oh_adm_ane_ind_time_rel','adm_id','ind_id','Induction'),
        'ot_anesthetic_maintenance' : fields.many2many('isf.oh.anesthetic.time.data','isf_oh_adm_ane_main_time_rel','adm_id','anm_id','Maintenance'),
        'ot_observation_ids' : fields.many2many('isf.oh.operation.observation','isf_oh_adm_obs_rel','adm_id','obs_id','Observarions'),
        'ot_intra_op_drugs_ids':  fields.many2many('isf.oh.operation.drugs','isf_oh_adm_op_drugs','adm_id','drug_id','Intra-operative Drugs'),
        # Surgery Notes
        'end_of_surgery' : fields.datetime('End Of Surgery'),
        'ebl' : fields.float('EBL (Estimated Blood Loss)'),
        'patient_condition_id' : fields.many2one('isf.oh.operation.patient.condition','Patient Condition'),
        'special_note' : fields.text('Special Instructions'),
        # Employee
        'doctor_id' : fields.many2one('hr.employee','Doctor'),
        'anesthesist_id' : fields.many2one('hr.employee','Anesthesist'),
        'assistant_id' : fields.many2one('hr.employee','Assistant'),
        # Diagnosis
        'pre_operative_diagnosis_id' : fields.many2one('isf.oh.disease','Pre Operative Diagnosis'),
        'post_operative_diagnosis_id' : fields.many2one('isf.oh.disease','Post Operative Diagnosis'),
        # Surgery Notes
        'surgery_procedure_id' : fields.many2many('isf.oh.operation.note','isf_oh_op_surg_proc_rel','adm_id','proc_id','Indication for surgical procedure'),
        'surgery_procedure_report_id' : fields.many2many('isf.oh.operation.note','isf_oh_op_surg_proc_report_rel','adm_id','proc_id','Procedure Report'),
        'surgery_procedure_post_op_id' : fields.many2many('isf.oh.operation.note','isf_oh_op_surg_post_rel','adm_id','proc_id','Post Operative Orders'),
    }
    
    _defautls = {
        'state' : 'new',
    }

class isf_oh_operation_type(osv.Model):
    _name = 'isf.oh.operation.type'
    
    _columns = {
        'name' : fields.char('Operation Type'),
    }

class isf_oh_operation(osv.Model):
    _inherits = {
        'product.product' : 'product_id'
    }
    _name = 'isf.oh.operation'
    
    _columns = {
        'product_id': fields.many2one('product.product', required=True,string='Related Product', ondelete='cascade',help='Product-related data of the operation'),
        'operation_type' : fields.many2one('isf.oh.operation.type','Operation Type'),
        'operations' : fields.boolean('Operations'),
    }
    
    _defaults = {
        'type' : 'service',
        'operations' : True,
        'sale_ok' : True,
        #'purchase_ok' : False,
    }
    
class isf_oh_anesthetic_method(osv.Model):
    _name = 'isf.oh.anesthetic.method'
    
    _columns = {
        'name' : fields.char('Name'),
    }
    
class isf_oh_anesthetic_time_data(osv.Model):
    _name = 'isf.oh.anesthetic.time.data'
    
    _columns = {
        'time' : fields.float('Time'),
        'note' : fields.char('Note',size=128),
    }
    
class isf_oh_operation_observation(osv.Model):
    _name = 'isf.oh.operation.observation'
    
    _columns = {
        'time' : fields.float('Time'),
        'bp' : fields.float('BP (mmhg)'),
        'pulse' : fields.float('PULSE/min'),
    }
    
class isf_oh_operation_patient_condition(osv.Model):
    _name = 'isf.oh.operation.patient.condition'
    
    _columns = {
        'name' : fields.char('Condition'),
    }
    
class isf_oh_operation_drugs(osv.Model):
    _name = 'isf.oh.operation.drugs'
    
    _columns = {
        'product_id' : fields.many2one('product.product','Drugs'),
        'quantity' : fields.integer('Qty'),
    }
    
class isf_oh_operation_note(osv.Model):
    _name = 'isf.oh.operation.note'
    _rec_name = 'note'
    
    _columns = {
        'note' : fields.char('Note')
    }