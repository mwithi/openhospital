from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv
import time
from datetime import datetime
import openerp

_debug=False
_logger = logging.getLogger(__name__)

class account_invoice(osv.osv):
	_inherit = 'account.invoice'

	def confirm_paid(self, cr, uid, ids, context=None):
		ret = super(account_invoice,self).confirm_paid(cr, uid, ids, context=context)
 		data = self.browse(cr, uid, ids)[0]
 		enc_o = self.pool.get('isf.oh.encounter')
 		enc_ids = enc_o.search(cr, uid, [('invoice_id','=',data.id)])

 		for encounter in enc_o.browse(cr, uid, enc_ids):
 			enc_o.write(cr, uid,[encounter.id],{'state':'close'})
 			for item in encounter.item_list:
		 		# Manage OPD
		 		opd_o = self.pool.get('isf.oh.opd')
		 		opd_ids = opd_o.search(cr, uid, [('opd_sequence','=',item.name)])
		 		for opd in opd_o.browse(cr, uid, opd_ids):
		 			_logger.debug('OPD Payed : %s',item.name)

		 		# Manage Exam Request
		 		req_o = self.pool.get('isf.oh.exam.request')
		 		req_ids = req_o.search(cr, uid, [('request_id','=',item.name)])
		 		for req in req_o.browse(cr, uid, req_ids):
					_logger.debug('Exam Request Payed : %s',item.name)
					req.set_to_pending()

				# Manage Admission
				adm_o = self.pool.get('isf.oh.admission')
				adm_ids = adm_o.search(cr, uid, [('admission_sequence','=',item.name)])
				for admission in adm_o.browse(cr, uid, adm_ids):
					_logger.debug('Admission Payed : %s',item.name)

 		return ret