from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv
import datetime
from datetime import date, timedelta
import time
import pytz
import re
from dateutil.relativedelta import relativedelta
from calendar import monthrange
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)
_debug=True

class isf_oh_patient(osv.Model):
    _description = "ISF OH Patient"
    _inherits = {
        'res.partner' : 'partner_id'
    }
    #_inherit = 'res.partner'
    _name = "isf.oh.patient"
       
    def _get_blood_selection(self, cursor, user_id, context=None):
        return (
            ('u','Unknown'),
            ('a', 'A'),
            ('b', 'B'),
            ('z', '0'),
            ('c','AB'))
            
    def _get_blood_sign_selection(self, cr, iud, context=None):
        return (
            ('u', 'Unknown'),
            ('p', 'Positive'),
            ('n', 'Negative'))
            
    def _get_yes_no_selection(self, cr, iud, context=None):
        return (
            ('u', 'Unknown'),
            ('y', 'Yes'),
            ('n', 'No'))
            
    def _get_parents_status(self, cr, iud, context=None):
        return (
            ('u', 'Unknown'),
            ('a', 'Alive'),
            ('d', 'Dead'))
            
    def _get_sex_selection(self, cr, iud, context=None):
        return (
            ('m', 'Male'),
            ('f', 'Female'))
        
    def _get_age_from_bdate(self, cr, uid, ids, field_names, arg, context):
        result = {}
        
        today = datetime.datetime.now()
        data = self.browse(cr, uid, ids)
        
        if _debug:
            _logger.debug('data : %s', data)
        
        for patient in data:
        
            bdate = datetime.datetime.strptime(patient.birth_date, '%Y-%m-%d')
            delta = relativedelta(today, bdate)
        
            if _debug:
                _logger.debug('patient : %s', patient.id)
                _logger.debug(' age_yy : %s', delta.years)
                _logger.debug(' age_mm : %s', delta.months)
                _logger.debug(' age_dd : %s', delta.days)
        
            result[patient.id] = {
                'age_yy_view' : delta.years,
                'age_mm_view' : delta.months,
                'age_dd_view' : delta.days,
            }
            
        if _debug:
            _logger.debug('result : %s', result)
        
        return result
    
    def _set_bdate_from_age(self, cr, uid, id, field_name, field_value, args=None, context=None):
        if _debug:
            _logger.debug('%s : %s', field_name, field_value)
    
        return True
        

    _columns = {
        #'state' : fields.selection([('active','Active'),('inactive','Inactive')],'state'),
        'patient_name' : fields.char('Patient Name',required=True),
        'previous_code' : fields.char('Previous Code', required=True),
        'partner_id': fields.many2one('res.partner', required=True,string='Related Partner', ondelete='restrict',help='Partner-related data of the patient'),
        'blood_type' : fields.selection(_get_blood_selection,'Blood Type', required=False), 
        'blood_sign' : fields.selection(_get_blood_sign_selection,'Sign', required=False),
        'next_kin' : fields.char('Next Kin',size=64),
        'sex' : fields.selection(_get_sex_selection,'Sex', required=True, store=True),
        'insurance' : fields.selection(_get_yes_no_selection,'Has Insurance', required=False),
        'note' : fields.text('Note', required=False),
        'date_type' : fields.selection([('date','Birth Date'),('age','Age')],'Date Type'),
        'view_age' : fields.boolean('Age'),
        'view_date' : fields.boolean('Date'),
        'birth_date' : fields.date('Birth Date', required=False),
        'age_yy_view' : fields.function(_get_age_from_bdate, fnct_inv=_set_bdate_from_age,type="integer", multi="age", string="Years"),
        'age_mm_view' : fields.function(_get_age_from_bdate, fnct_inv=_set_bdate_from_age,type="integer", multi="age", string="Months"),
        'age_dd_view' : fields.function(_get_age_from_bdate, fnct_inv=_set_bdate_from_age,type="integer", multi="age", string="Days"),
        'father_name' : fields.char('Father Name', size=64, required=False),
        'mother_name' : fields.char('Mother Name', size=64, required=False),
        'father_status' : fields.selection(_get_parents_status,'Father status', required=False),
        'mother_status' : fields.selection(_get_parents_status,'Mother status', required=False),
        'parents_together' : fields.selection(_get_yes_no_selection,'Parents Together', required=False),
        'admission_ids' : fields.one2many('isf.oh.admission','patient_id','Admissions'),
        'opd_ids' : fields.one2many('isf.oh.opd','patient_id','Clinical Sheet'),
        'registration_date' : fields.datetime('Registration Date'),
        'lab_history_ids' : fields.one2many('isf.oh.lab.history','patient_id','Exams History',order='date,exam_id'),
        'place_id' : fields.many2one('isf.oh.place','Place'),
        # Finger Print
        'finger_print_1' : fields.binary('Finger Print 1'),
        'finger_print_2' : fields.binary('Finger Print 2'),
        'finger_print_3' : fields.binary('Finger Print 3'),
        'finger_print_4' : fields.binary('Finger Print 4'),
        'finger_print_5' : fields.binary('Finger Print 4'),
        # Allergy
        'allergy_ids' : fields.many2many('isf.oh.allergy','isf_oh_pat_alg_rel','pat_id','allergy_id','Allergy'),
    }
    
    _defaults = {
        'date_type' : 'date',
        'registration_date' : lambda *a: time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
        'view_date' : True,
        'date_type' : 'date',
    }
    
    def _check_date_not_in_future(self, cr, uid, ids, context=None):
        isf_oh_utils = self.pool.get('isf.open.hospital.utils')
        check = False
        for obj in self.browse(cr, uid, ids):
            check = isf_oh_utils.check_date_not_in_future(cr, uid, obj.birth_date)
            
        return check
    
    _constraints = [
        (_check_date_not_in_future, 'Birth Date in the future not allowed.', ['birth_date'])
    ]
    
    
    def _default_get(self, cr, uid, fields, context):
        if context is None:
            context = {}
        res = super(isf_oh_patient, self).default_get(cr, uid, fields, context=context)
        
        return res;
        
    def _get_prefix(self):
        now = datetime.datetime.now()
        year_code = now.strftime("%Y")
            
        return year_code[2:]
        
    
    def create(self, cr, uid, vals, context=None):
        if _debug:
            _logger.debug('Create a new patient')
        
        
        obj_sequence = self.pool.get('ir.sequence')
        obj_period = self.pool.get('account.period')
        seq_ids = obj_sequence.search(cr, uid, [('name','=','Patient')])
        date = datetime.datetime.now()
        period_ids = obj_period.find(cr, uid, date, context=context)
        period = obj_period.browse(cr, uid, period_ids)[0]
        ctx = context.copy()
        ctx.update({
            'fiscalyear_id': period.fiscalyear_id.id,
        })
        new_seq = obj_sequence.next_by_id(cr, uid, seq_ids,ctx)
        
        vals['name'] = new_seq
        
        res_id = super(isf_oh_patient, self).create(cr, uid, vals, context=context)  
        
        return res_id
    
    def check_encounter(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
            
        this = self.browse(cr, uid, ids)[0]
            
        enc_obj = self.pool.get('isf.oh.encounter')
        enc_id = enc_obj.find_open_encounter_by_patient(cr, uid, this.id)
        
        # No encounter open
        if not enc_id:
            if context.get('source') == 'opd':
                return self.open_opd_form(cr, uid, ids, context)
            else:
                return self.open_admission_form(cr, uid, ids, context)
        
        # Found encounter open: open dialog window    
        else:
            context.update({
                'encounter_id' : enc_id,
            })
            
            mod_obj = self.pool.get('ir.model.data')
            res = mod_obj.get_object_reference(cr, uid, 'isf_open_hospital', 'isf_check_encounter')
            
            result = {
                'name': 'Open Encounter',
                'view_type': 'form',
                'view_mode': 'form',
                'view_id': [res and res[1] or False],
                'res_model': 'isf.check.encounter',
                'context': context,
                'type': 'ir.actions.act_window',
                'target': 'new',
            }
        
            return result
        
        return None
    
    def _search_for_older_admission(self, cr, uid, ids, adm_seq, context=None):
        adm_o = self.pool.get('isf.oh.admission')
        adm_ids = adm_o.search(cr, uid, [('admission_sequence','=',adm_seq)])
        if len(adm_ids) == 0:
            return False
        
        return adm_o.browse(cr, uid, adm_ids)[0].id
    
    def open_admission_form(self, cr, uid, ids, context=None):
        if _debug:
            _logger.debug('==> open_admission_form')
        if context is None:
            context = {}
            
        this = self.browse(cr, uid, ids)[0]
        enc_id = context.get('encounter_id')
        adm_seq = context.get('source')
        res_id = self._search_for_older_admission(cr, uid, ids, adm_seq, context=context)
        
        _logger.debug('res_id : %s', res_id)
        
        mod_obj = self.pool.get('ir.model.data')
        res = mod_obj.get_object_reference(cr, uid, 'isf_open_hospital', 'isf_oh_admission_form')
    
#         obj_sequence = self.pool.get('ir.sequence')
#         obj_period = self.pool.get('account.period')
#         seq_ids = obj_sequence.search(cr, uid, [('name','=','Admission')])
#         date = datetime.datetime.now()
#         period_ids = obj_period.find(cr, uid, date, context=context)
#         period = obj_period.browse(cr, uid, period_ids)[0]
#         ctx = context.copy()
#         ctx.update({
#             'fiscalyear_id': period.fiscalyear_id.id,
#         })
#         new_seq = obj_sequence.next_by_id(cr, uid, seq_ids[0],ctx)
# 
#         # Create Encounter
#         enc_o = self.pool.get('isf.oh.encounter')
#         enc_id = enc_o.create(cr, uid, {'source' : new_seq})
#         enc_o.add_event_to_encounter(cr, uid, enc_id, new_seq)
    
        ctx = context.copy()
        if not ctx.get('patient_id'):
            ctx.update({
                'patient_id' : this.id,
#                 'type' : opd_type,
#                 'opd_sequence' : new_seq,
                'encounter_id' : enc_id,
            })
        
        if _debug:
            _logger.debug('ctx : %s', ctx)
    
        result = {
            'name': 'Admission/Discharge Form',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res and res[1] or False],
            'res_model': 'isf.oh.admission',
            'context': ctx,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': res_id, 
        }
        
        return result
        
    def _get_account_enabled(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.oh.settings','account_enabled')
    
    def _search_for_older_opd(self, cr, uid, ids, opd_seq, context=None):
        opd_o = self.pool.get('isf.oh.opd')
        opd_ids = opd_o.search(cr, uid, [('opd_sequence','=',opd_seq)])
        if len(opd_ids) == 0:
            return False
        
        return opd_o.browse(cr, uid, opd_ids)[0].id
    
    def open_opd_form(self, cr, uid, ids, context=None):
        if _debug:
            _logger.debug('==> open_opd_form')
        if context is None:
            context = {}
            
        if _debug:
            _logger.debug('Context : %s', context)
            
        this = self.browse(cr, uid, ids)[0]
        enc_id = context.get('encounter_id')
        opd_seq = context.get('source')
        if _debug:
            _logger.debug('opd_seq <= source : %s', opd_seq)
        
        res_id = self._search_for_older_opd(cr, uid, ids, opd_seq)
        if _debug:
            _logger.debug('res_id : %s', res_id)
            
#         opd_type = 'save'
#         account_enabled = self._get_account_enabled(cr, uid)
#         if account_enabled:
#             opd_type = 'next'
            
        mod_obj = self.pool.get('ir.model.data')
        res = mod_obj.get_object_reference(cr, uid, 'isf_open_hospital', 'isf_oh_opd_form')
        
#         obj_sequence = self.pool.get('ir.sequence')
#         obj_period = self.pool.get('account.period')
#         seq_ids = obj_sequence.search(cr, uid, [('name','=','OPD')])
#         date = datetime.datetime.now()
#         period_ids = obj_period.find(cr, uid, date, context=context)
#         period = obj_period.browse(cr, uid, period_ids)[0]
#         ctx = context.copy()
#         ctx.update({
#             'fiscalyear_id': period.fiscalyear_id.id,
#         })
#         new_seq = obj_sequence.next_by_id(cr, uid, seq_ids[0],ctx)
#     
#         # Create Encounter
#         enc_o = self.pool.get('isf.oh.encounter')
#         enc_id = enc_o.create(cr, uid, {'source' : new_seq})
#         enc_o.add_event_to_encounter(cr, uid, enc_id, new_seq)
       
        ctx = context.copy()
        if not ctx.get('patient_id'):
            ctx.update({
                'patient_id' : this.id,
#                 'type' : opd_type,
#                 'opd_sequence' : new_seq,
                'encounter_id' : enc_id,
            })
        
        if _debug:
            _logger.debug('ctx : %s', ctx)

        result = {
            'name': 'OPD Form',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res and res[1] or False],
            'res_model': 'isf.oh.opd',
            'context': ctx,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': res_id, 
        }
        
        return result
        
    def open_exam_wizard_form(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
            
        this = self.browse(cr, uid, ids)[0]
            
        mod_obj = self.pool.get('ir.model.data')
        res = mod_obj.get_object_reference(cr, uid, 'isf_open_hospital', 'isf_oh_exam_wizard_form')
    
        ctx = context.copy()
        ctx.update({
            'patient_id' : this.id,
        })
    
        result = {
            'name': 'Exams Wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res and res[1] or False],
            'res_model': 'isf.oh.exam.wizard',
            'context': ctx,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'res_id': False, 
        }
        
        return result
    
    def onchange_date_type(self, cr, uid, ids, date_type,context=None):
        result = {'value': {}}
    
        if date_type == 'date':
            result['value'].update({
                'view_date' : True,
                'view_age' : False,
            })
        elif date_type == 'age':
            result['value'].update({
                'view_date' : False,
                'view_age' : True,
            })
        return result
    
    def onchange_age(self, cr, uid, ids, age_dd, age_mm, age_yy, context=None):
        result = {'value':{}}

        now = datetime.datetime.now()
        d0 = date(now.year,now.month,now.day)


        db = d0 - datetime.timedelta(days=age_dd)
        db = db - relativedelta( months = age_mm )
        db = db - relativedelta( months = (age_yy*12))

        date_str = db.strftime("%Y-%m-%d")
        
        if _debug:
            _logger.debug('birth_date : %s', date_str)
            
        result['value'].update({
            'birth_date' : date_str,
        })

        return result
    
    def onchange_birthdate(self, cr, uid, ids, birth_date, context=None):
        result = {'value': {}}
        
        today = datetime.datetime.now()
        bdate = datetime.datetime.strptime(birth_date, '%Y-%m-%d')
        delta = relativedelta(today, bdate)
        
        if _debug:
            _logger.debug('age_yy : %s', delta.years)
            _logger.debug('age_mm : %s', delta.months)
            _logger.debug('age_dd : %s', delta.days)
        
        result['value'].update({
            'age_yy_view' : delta.years,
            'age_mm_view' : delta.months,
            'age_dd_view' : delta.days,
        })
        
        return result

    def view_patient(self, cr, uid, ids, context=None):
        if _debug:
            _logger.debug('==> OPEN PATIENT')
        
        this = self.browse(cr, uid, ids)[0]
        pat = this.patient_id

        mod_obj = self.pool.get('ir.model.data')
        res = mod_obj.get_object_reference(cr, uid, 'isf_open_hospital', 'isf_oh_patient_form')   
        ctx = context.copy() 
        result = {
            'name': 'Patient',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res and res[1] or False],
            'res_model': 'isf.oh.patient',
            'context': ctx,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': pat.id, 
        }   
        return result
        
isf_oh_patient()

class isf_check_encounter(osv.osv_memory):
    _name = "isf.check.encounter"
    _description = "Check Open Encounter"
    
    def show_encounter(self, cr, uid, ids, context=None):
        if _debug:
            _logger.debug('==> show_encounter')
        
        enc_id = context.get('encounter_id')
        pat_obj = self.pool.get('isf.oh.patient')
        enc_obj = self.pool.get('isf.oh.encounter')
        
        enc = enc_obj.browse(cr, uid, enc_id)
        
        context.update({ 
            'source' : enc.source,
        })
        
        if _debug:
            _logger.debug('Context : %s', context)
        
        if enc.source.startswith('OPD'):
            return pat_obj.open_opd_form(cr, uid, ids, context)
        else:
            return pat_obj.open_admission_form(cr, uid, ids, context)
        
    def close_encounter(self, cr, uid, ids, context=None):
        if _debug:
            _logger.debug('==> show_encounter')
        
        enc_id = context.get('encounter_id')
        pat_obj = self.pool.get('isf.oh.patient')
        enc_obj = self.pool.get('isf.oh.encounter')
        
        enc = enc_obj.browse(cr, uid, enc_id)
        enc_obj.write(cr, uid, [enc_id], {'state' : 'close'})
        
        context.update({ 
            'source' : None,
        })
        
        if _debug:
            _logger.debug('Context : %s', context)
        
        if enc.source.startswith('OPD'):
            return pat_obj.open_opd_form(cr, uid, ids, context)
        else:
            return pat_obj.open_admission_form(cr, uid, ids, context)
        