from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_oh_patient_number_wizard(osv.osv_memory):
    _name = "isf.oh.patient.number.wizard"
    _description = "Patient Statistics"
    
    _columns = {
        'date_start' : fields.date('Date Start'),
        'date_stop' : fields.date('Date Stop'),
        'group_type' : fields.selection([('month','Group By Month'),('day','Group By Day')],'Group Type'),
    }

    _defaults = {
        'group_type' : 'day',
    }
    
    
isf_oh_patient_number_wizard()
