from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv
import datetime
from datetime import date
import time
import pytz
import re

_debug=False
_logger = logging.getLogger(__name__)

class isf_oh_patient_search(osv.TransientModel):
    _name = 'isf.oh.patient.search'
    
    _columns = {
        # Filters
        'filter_patient_code' : fields.char('Code'),
        'filter_patient_previous_code' : fields.char('Previous Code'),
        'filter_patient_name' : fields.char('Name'),
        'filter_patient_sex' : fields.selection([('m','Male'),('f','Female')],'Sex'),
        # Data
        'patient_ids' : fields.many2many('isf.oh.patient','isf_oh_pat_search_rel','search_id','pat_id','Patients'),
    }
    
        
    def view_patient(self, cr, uid, ids, context=None):
        if _debug:
            _logger.debug('==> OPEN PATIENT')
        
        this = self.browse(cr, uid, ids)[0]
        pat = this.patient_id

        mod_obj = self.pool.get('ir.model.data')
        res = mod_obj.get_object_reference(cr, uid, 'isf_open_hospital', 'isf_oh_patient_form')   
        ctx = context.copy() 
        result = {
            'name': 'Patient',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': [res and res[1] or False],
            'res_model': 'isf.oh.patient',
            'context': ctx,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'current',
            'res_id': pat.id, 
        }   
        return result
        
        
    def onchange_filter(self,cr,uid,ids,patient_id,previous_code,patient_name,patient_sex,context=None):
        result = {'value' : {}}
        
        str_query = "select p.id,r.name,p.sex,p.patient_name from isf_oh_patient p, res_partner r where p.partner_id = r.id "
        
        if patient_id:
            str_query += "and r.name like '%"+patient_id+"%'"
            
        if previous_code:
            str_query += "and p.previous_code like '%"+previous_code+"%'"
        
        if patient_name:
            str_query += " and p.patient_name ilike '%"+patient_name+"%'"
        
        if patient_sex:
            str_query += " and p.sex like '%"+patient_sex+"%'"
            
        cr.execute(str_query)
        res = cr.fetchall()
        
        patient_id = None
        res_ids = []
        for data in res:
            res_ids.append(data[0])
            patient_id = data[0]
        
        result['value'].update({
            'patient_ids' : res_ids,
        })
            
        return result
    
    def open_finger_print_search(self, cr, uid, ids, context=None):
        return False
        #gateway = JavaGateway()
        #this = self.browse(cr, uid, ids)[0]
        #if _debug:
        #    _logger.debug('Gateway : %s', gateway)
        
        #gateway.entry_point.createAndShowGUI('dummy','search')
        #result = {'value' : {}}
        
        #if gateway.entry_point.patientFound():
        #    patient_id = gateway.entry_point.getPatientId()
        #    pat_o = self.pool.get('isf.oh.patient')
        #    pat_ids = pat_o.search(cr, uid, [('id_number','=',patient_id)])
        #    pat = pat_o.browse(cr, uid, pat_ids)[0]
            
        #    if _debug:
        #        _logger.debug('FP Search : %s, %s', patient_id,pat.id)
        #    mod_obj = self.pool.get('ir.model.data')
        #    res = mod_obj.get_object_reference(cr, uid, 'isf_open_hospital', 'isf_oh_patient_search_form')   
        #    ctx = context.copy() 
        #    ctx.update({
        #        'patient_id' : pat.id,
        #    })
        #    result = {
        #        'name': 'Patient Search',
        #        'view_type': 'form',
        #        'view_mode': 'form',
        #        'view_id': [res and res[1] or False],
        #        'res_model': 'isf.oh.patient.search',
        #       'context': ctx,
        #       'type': 'ir.actions.act_window',
        #       'nodestroy': True,
        #       'target': 'new',
        #       #'res_id': this.id, 
        #   }   
        #   return result
        #return False
        
        