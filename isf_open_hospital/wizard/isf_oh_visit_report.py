from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_oh_visit(osv.osv_memory):
    _name = "isf.oh.visit"
    _description = "Visit Module"
    
    def _get_module_type(self, cr, uid, context=None):
        return (
            ('c', 'Child'),
            ('a', 'Adult'))
            
    def _get_ward(self, cr, uid, context=None):
        return (
            ('w1', 'Ward1'),
            ('w2', 'Ward2'))
    
    _columns = {
        'module_type': fields.selection(_get_module_type, "Module Type", required=True,select=True),
        'ward' : fields.many2one('hr.department','Ward',required=True),
    }
    
    
isf_oh_visit()
