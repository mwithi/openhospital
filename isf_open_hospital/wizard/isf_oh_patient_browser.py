from openerp.osv import fields, osv
from tools.translate import _
import logging
from openerp.osv import fields, osv
import datetime
from datetime import date
import time

_debug=False
_logger = logging.getLogger(__name__)

class isf_oh_patient_browser(osv.TransientModel):
    _name = 'isf.oh.patient.browser'
    
    _columns = {
        #filter
        'admission_status' : fields.selection([('admitted','Admitted'),('not_admitted','Not Admitted')],'Admission Status'),
        'code' : fields.char('Code'),
        'name' : fields.char('Name'),
        'sex' : fields.selection([('a','All'),('m','Male'),('f','Female')],'Sex'),
        'count' : fields.integer('Count'),
        # Line Ids
        'line_ids' : fields.one2many('isf.oh.patient','id_number','Patients'),
    }
    
    _defaults = {
        'sex' : 'a',
    }
    
    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(isf_oh_patient_browser, self).default_get(cr, uid, fields, context=context)
     
        res['line_ids'] = self._load_patients(cr, uid,context=context)
        res['count'] = len(res['line_ids'])
     
        return res
     
    def _load_patients(self, cr, uid,context=None):
        if context is None:
            context = {}
            
        pat_o = self.pool.get('isf.oh.patient')
        pat_ids = pat_o.search(cr, uid, [])
        line_o = self.pool.get('isf.oh.patient.browser.line')
        
        ids = []
        ctx = {}
        for pat in pat_o.browse(cr, uid, pat_ids):
           ids.append(pat.id)
        
        return ids
        
    def filter_patient(self, cr, uid, ids, admission_status, code, name, sex, context=None):
        if context is None:
            context = {}
        
        str_query = "SELECT pat.id FROM isf_oh_patient pat, res_partner res where pat.partner_id = res.id"
        
        if code:
            str_query += " and pat.id_number like '%"+code+"%'"
            
        if name:
            str_query += " and partner.name like '%"+name+"%'"
            
        if sex != 'a':
            str_query += " and sex like '%"+sex+"%'"
            
        cr.execute(str_query)
        res_list = cr.fetchall()
        
        ctx = {}
        for res in res_list:
            if _debug:
                _logger.debug('RES : %s',res)
            ids.append(res[0])
        
        result = {'value' : {}}
        result['value'].update({
            'line_ids' : ids,
        })
        return result
        