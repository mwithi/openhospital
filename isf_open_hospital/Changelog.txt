OPEN HOSPITAL CORE
==================

2014-11-09
----------

* Version 0.4
* Add OPD Report Module
* OPD
    - Integrated with exams request
* Exam Request
    - Add ref field
* Add IPD Report Module
* Add OT Report Module

2014-09-10:
-----------

* Version 0.3
* Add Exams History management in Patient module
* Add CSV auto install 