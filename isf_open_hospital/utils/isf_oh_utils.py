import datetime
import logging

from openerp.osv import fields, osv
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT, \
    DEFAULT_SERVER_DATE_FORMAT

from tools.translate import _


_logger = logging.getLogger(__name__)
_debug=True

class isf_oh_utils(osv.osv_memory):
    _name = 'isf.open.hospital.utils'
    
    def check_datetime_not_in_future(self, cr, uid, aDatetime, context=None):
        date_to_check = datetime.datetime.strptime(aDatetime, DEFAULT_SERVER_DATETIME_FORMAT)  #'%Y-%m-%d')
        today_date = datetime.datetime.now()
        _logger.info('datetime.datetime.strptime: %s', date_to_check)
        _logger.info('datetime.datetime.now(): %s', today_date)
        
        if date_to_check > today_date:
            return False
        return True
    
    def check_date_not_in_future(self, cr, uid, aDate, context=None):
        date_to_check = datetime.datetime.strptime(aDate, DEFAULT_SERVER_DATE_FORMAT) #'%Y-%m-%d')
        today_date = fields.datetime.context_timestamp(cr, uid, datetime.datetime.now(), context=context)
        _logger.info('date_to_check: %s', date_to_check)
        _logger.info('today_date: %s', today_date)
        
        if date_to_check.date() > today_date.date():
            return False
        return True
        
    def check_me(self, cr, uid, ids, context=None):
        context = context or {}
        _logger.info("self %s IDS %s ctx %s" % (self, ids, context))
        
        return True
    
isf_oh_utils()