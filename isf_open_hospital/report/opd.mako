<html>
<head>
    <style type="text/css">
        ${css}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    font-family:verdana;
    font-size:10px;
    padding: 5px;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.td-data {
    border: 0px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:11px;
}

.th-data {
    font-family:verdana;
    font-size:10px;
}
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print first_page()
    """
    %>

    <center>
        <h3>OPD EXAMINATION FORM</h3>
    </center>
    <table class="table-data">
    %for data in first_page():
        <table class="table-data">
            <tr>
                <th class="td-data" style="width: 10%;">Date</th>
                <th class="td-data" style="width: 10%;">Patient</th>
                <th class="td-data" style="width: 10%;">Name</th>
                <th class="td-data" style="width: 10%;">Age</th>
                <th class="td-data" style="width: 10%;">Sex</th>
                <th class="td-data" style="width: 10%;">OPD</th>
            </tr>
            <tr>
                <th class="td-data" style="width: 15%;">${data['date']}</th>
                <td class="td-data" style="width: 10%;">${data['code']}</td>
                <td class="td-data" style="width: 25%;">${data['name']}</td>
                <td class="td-data" style="width: 10%;text-align:right;">${data['age']}</td>
                <td class="td-data" style="width: 10%;">${data['sex']}</td>
                <td class="td-data" style="width: 10%;">${data['card_id']}</td>
            </tr>
        </table>
        
        <br/>
        <table class="table-data">
            <tr>
                <th style="text-align:left">Presenting Compliant(s)</th>
            </tr>
            <tr>
                <td class="td-data" style="width: 10%;">${data['present_compliant']}</td>
            </tr>
        </table>
        
        <table class="table-data">
            <tr>
                <th style="text-align:left">Observation and Findigs</th>
            </tr>
            <tr>
                <td class="td-data" style="width: 10%;">${data['observation']}</td>
            </tr>
        </table>
        
        <table class="table-data">
            <tr>
                <th style="text-align:left">Summary and Interpretation of findings</th>
            </tr>
            <tr>
                <td class="td-data" style="width: 10%;">${data['summary']}</td>
            </tr>
        </table>
        <!-- ############################### Investigation ############################### -->
        <br/>
        <table class="table-data-nb" style="width: 100%;">
            <tr>
                <th style="text-align:left" colspan="3">Investigation and Procedure Requested</th>
            </tr>
            <tr>
                <td style="width: 50%;" valign="top">
                    <table class="table-data">
                        <tr>
                            <th style="text-align:left">Laboratory</th>
                        </tr>
                        %for exam in data['exams']:
                        <tr>
                            <td class="td-data">${exam}</td>
                        </tr>
                        %endfor
                    </table>
                </td>
                <td style="width: 50%;" valign="top">
                    <table class="table-data">
                        <tr>
                            <th style="text-align:left">Others</th>
                        </tr>
                        %for other in data['others']:
                        <tr>
                            <td class="td-data">${other}</td>
                        </tr>
                        %endfor
                    </table>
                </td>
            </tr>
        </table>
        <br/>
        <!-- ############################### Diagnosis ############################### -->
        <table class="table-data" style="width: 100%;">
            <tr>
                <th style="text-align:left" colspan="2">Diagnosis</th>
            </tr>
            <tr>
                <td class="td-data" colspan="2">${data['disease']}</td>
            </tr>
                %for diag in data['diagnosis']:
                    <tr>
                        <td class="td-data">${diag}</td>
                    </tr>
                %endfor
        </table>
        <!-- ############################### Orders ############################### -->
        <br/>
        <table class="table-data-nb" style="width: 100%;">
            <tr>
                <td valign="top">
                    <table class="table-data" style="width: 100%;">
                        <tr>
                            <th>Action Taken</th>
                        </tr>
                        <tr>
                            <td class="td-data">${data['action_taken_desc']}</td>
                            %if data['action_taken'] == 'fu':
                                <td class="td-data">${data['follow_up_days']}</td>
                            %endif
                        </tr>
                    </table>
                </td>
                <td>
                    <table class="table-data" style="width: 100%;">
                        <tr>
                            <th>Orders</th>
                        </tr>
                        %for order in data['orders']:
                            <tr>
                                <td class="td-data">${order}</td>
                            </tr>
                        %endfor
                    </table>
                </td>
            </tr>
        </table>
        
        <br/>
        <!-- ############################### Doctor and signature ############################### -->
        <table class="table-data-nb" style="width: 100%;">
            <tr>
                <td class="td-data-nb" style="width: 25%;">Examiner</td>
                <td class="td-data-nb" style="width: 25%;">${data['doctor']}</td>
                <td class="td-data-nb" style="width: 25%;text-align:right">Signature</td>
                <td class="td-data-nb" style="width: 25%;text-align:right">............................</td>
            </tr>
        </table>
        
        <!-- ############################### Orders and Prescriptions ############################### -->
        <br/>
        <div style="height:20px;">
        <font style="font-family:verdana;font-size:8px;">Cut here for Pharmacy</font>
        <hr style="border: 1px dashed #000;" />
        </div>
        
        <br/>
        
         <table class="table-data">
            <tr>
                <th class="td-data" style="width: 10%;">Date</th>
                <th class="td-data" style="width: 10%;">Patient</th>
                <th class="td-data" style="width: 10%;">Doctor</th>
            </tr>
            <tr>
                <th class="td-data" style="width: 10%;">${data['date']}</th>
                <td class="td-data" style="width: 10%;">${data['code']}</td>
                <td class="td-data" style="width: 10%;">${data['doctor']}</td>
            </tr>
        </table>
        <br>
         <table class="table-data">
            <tr>
                <th class="td-data" colspan="2">Prescription</th>
            </tr>
            %for line in data['drugs']:
                <tr>
                    <td class="td-data" style="width=90%">${line['drug']}</td>
                    <td class="td-data" style="width=10%">${line['qty']}</td>
                </tr>
            %endfor
         </table>
    %endfor

</body>
</html>