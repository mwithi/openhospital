# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug=False

class isf_oh_visit_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.oh.visit.webkit'

    def __init__(self, cr, uid, name, context=None):
        super(isf_oh_visit_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'headerlines' : self.headerlines,
        })
        self.context = context
        self.result_acc = []
        self.result_header = []
        self.result_cheque = []

    """"
    def set_context(self, objects, data, ids, report_type=None):
        for obj in objects:
            print obj, obj.id
        new_ids = ids
        return super(isf_bs_report_parser, self).set_context(objects, data, new_ids, report_type=report_type)
    """
    #def _add_header(self, node, header=1):
    #    if header == 0:
    #        self.rml_header = ""
    #    return True
    
    
    def _get_bs_object(self, ids):
        bs_obj = self.pool.get('isf.bank.account.checking')
        
        _logger.debug('_get_bs_object : %s',ids)
        
        bs = bs_obj.browse(self.cr, self.uid, ids)
        
        return bs
    
    def headerlines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.oh.patient')
        hr_dep_obj = self.pool.get('hr.department')
        
        if _debug:
            _logger.debug('CTX : %s',ctx)
        
        id_number = ctx.get('id_number')
        patient_name = ctx.get('name')
        ward_id = ctx.get('ward_id')
        sex = ctx.get('sex')
        hr_dep_ids = hr_dep_obj.search(self.cr, self.uid, [('id','=',ward_id)])
        dept = hr_dep_obj.browse(self.cr, self.uid, hr_dep_ids)[0] or None 
        date = '12/05/2014'
        
        res = {
            'id_number' : id_number,
            'name' : patient_name,
            'ward' : dept.name,
            'sex' : sex,
            'date' : date,
        }
        
        self.result_header.append(res)
        
        return self.result_header

            
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.bs.bank.checking')
        
        if _debug:
            _logger.debug('CTX : %s',ctx)
            
            
        #self.result_acc.append(res)
            
        return self.result_acc

report_sxw.report_sxw('report.isf.oh.visit.webkit', 'isf.oh.visit', 'addons/isf_open_hospital/report/isf_oh_visit_report.mako', parser=isf_oh_visit_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
