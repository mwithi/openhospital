# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import datetime

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug=False

class isf_oh_ot_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.oh.ot.webkit'

    def __init__(self, cr, uid, name, context=None):
        super(isf_oh_ot_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines' : self.lines,
            'inductions' : self.inductions,
            'maintenance' : self.maintenance,
            'observations' : self.observations,
            'intraoperative' : self.intraoperative,
            'footerdata' : self.footerdata,
            'employee': self.employee,
            'surgerynote' : self.surgerynote,
            'procedurereport' : self.procedurereport,
            'postord' : self.postord,
        })
        self.context = context
        self.first_acc = []
        self.induct_acc = []
        self.maintenance_acc = []
        self.observation_acc = []
        self.intra_op_acc = []
        self.employee_acc = []
        self.footerdata_acc = []
        self.surgerynote_acc = []
        self.procreport_acc = []
        self.postord_acc = []
        
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        op_o = self.pool.get('isf.oh.patient.operation')
        active_id = ctx.get('active_id')
        self.first_acc = []
        op = op_o.browse(self.cr, self.uid, [active_id])[0]
        
        bed_no = 'none'
        if op.admission_id.bed:
            bed_no = op.admission_id.bed.name
        res = {
            'patient_code' : op.admission_id.patient_id.id_number,
            'patient_age' : op.admission_id.patient_id.age,
            'patient_sex' : op.admission_id.patient_id.sex,
            'admission_code' : op.admission_id.admission_sequence,
            'date_of_admission' : op.admission_id.admission_date,
            'bed_no' : bed_no,
            'admission_code' : op.admission_id.admission_sequence,
            'date' : op.date,
            'ward' : op.ward_id.name,
            'operation' : op.operation_id.name,
            'ot_informed' :op.ot_informed,
            'ot_shaving' : op.ot_shaving,
            'ot_catheterizatiomn' : op.ot_catheterizatiomn,
            'ot_fasting' : op.ot_fasting,
            'ot_iv_line' : op.ot_iv_line,
            'anesthesist' : op.anesthesist_id.name,
            'doctor' : op.doctor_id.name,
            'method' : op.ot_anesthetic_method.name,    
            'allergy' : op.ot_allergy,
            'anesthetic_risk' : op.ot_anesthetic_risk,    
        }
        self.first_acc.append(res)
        return self.first_acc
        
        
    def inductions(self, ids=None, done=None):
        ctx = self.context.copy()
        op_o = self.pool.get('isf.oh.patient.operation')
        active_id = ctx.get('active_id')
        
        op = op_o.browse(self.cr, self.uid, [active_id])[0]
        
        for ind in op.ot_anesthetic_induction:
            res = {
                'index' : len(self.induct_acc)+1,
                'time' : ind.time,
                'note' : ind.note,
            }
            self.induct_acc.append(res)
        
        return self.induct_acc
        
    def maintenance(self, ids=None, done=None):
        ctx = self.context.copy()
        op_o = self.pool.get('isf.oh.patient.operation')
        active_id = ctx.get('active_id')
        
        op = op_o.browse(self.cr, self.uid, [active_id])[0]
        
        for main in op.ot_anesthetic_maintenance:
            res = {
                'index' : len(self.maintenance_acc)+1,
                'time' : main.time,
                'note' : main.note,
            }
            self.maintenance_acc.append(res)
        
        return self.maintenance_acc
        
    def observations(self, ids=None, done=None):
        ctx = self.context.copy()
        op_o = self.pool.get('isf.oh.patient.operation')
        active_id = ctx.get('active_id')
        
        op = op_o.browse(self.cr, self.uid, [active_id])[0]
        
        time_ids = []
        bp_ids = []
        pulse_ids = []
        for ob in op.ot_observation_ids:
            time_ids.append(ob.time)
            bp_ids.append(ob.bp)
            pulse_ids.append(ob.pulse)    
                
        res = {
            'time_ids' : time_ids,
            'bp_ids' : bp_ids,
            'pulse_ids' : pulse_ids,
        }
        self.observation_acc.append(res)
        
        return self.observation_acc
        
    def intraoperative(self, ids=None, done=None):
        ctx = self.context.copy()
        op_o = self.pool.get('isf.oh.patient.operation')
        active_id = ctx.get('active_id')
        
        op = op_o.browse(self.cr, self.uid, [active_id])[0]
        
        for drug in op.ot_intra_op_drugs_ids:
            res = {
                'index' : len(self.intra_op_acc)+1,
                'drug' : drug.product_id.name_template,
                'qty' : drug.quantity,
            }
            self.intra_op_acc.append(res)
        
        return self.intra_op_acc
    
    def employee(self, ids=None, done=None):
        ctx = self.context.copy()
        op_o = self.pool.get('isf.oh.patient.operation')
        active_id = ctx.get('active_id')
        
        op = op_o.browse(self.cr, self.uid, [active_id])[0]
        
        res = {
            'doctor' : op.doctor_id.name,
            'anesthesist' : op.anesthesist_id.name,
        }
        self.employee_acc.append(res)
        return self.employee_acc
        
    def footerdata(self, ids=None, done=None):
        ctx = self.context.copy()
        op_o = self.pool.get('isf.oh.patient.operation')
        active_id = ctx.get('active_id')
        
        op = op_o.browse(self.cr, self.uid, [active_id])[0]
        
        res = {
            'endos' : op.end_of_surgery,
            'ebl' : op.ebl,
            'codition' : op.patient_condition_id.name,
            'instructions' : op.special_note,
        }
        self.footerdata_acc.append(res)
        return self.footerdata_acc
        
    def surgerynote(self, ids=None, done=None):
        ctx = self.context.copy()
        op_o = self.pool.get('isf.oh.patient.operation')
        active_id = ctx.get('active_id')
        
        op = op_o.browse(self.cr, self.uid, [active_id])[0]
        
        surg_note_ids = []
        for surg in op.surgery_procedure_id:
            surg_note_ids.append(surg.note)
        
        res = {
            'prediag' : op.pre_operative_diagnosis_id.description,
            'postdiag' : op.post_operative_diagnosis_id.description,
            'doctor' : op.doctor_id.name,
            'anesthesist' : op.anesthesist_id.name,
            'assistant' : op.assistant_id.name,
            'date' : op.date,
            'surg_note_ids' : surg_note_ids,
        }
        self.surgerynote_acc.append(res)
        return self.surgerynote_acc
        
    def procedurereport(self, ids=None, done=None):
        ctx = self.context.copy()
        op_o = self.pool.get('isf.oh.patient.operation')
        active_id = ctx.get('active_id')
        
        op = op_o.browse(self.cr, self.uid, [active_id])[0]
        
        surg_note_ids = []
        for proc in op.surgery_procedure_report_id:
            res = {
                'note': proc.note,
            }
            self.procreport_acc.append(res)
        return self.procreport_acc
        
    def postord(self, ids=None, done=None):
        ctx = self.context.copy()
        op_o = self.pool.get('isf.oh.patient.operation')
        active_id = ctx.get('active_id')
        
        op = op_o.browse(self.cr, self.uid, [active_id])[0]
        
        surg_note_ids = []
        for post in op.surgery_procedure_post_op_id:
            res = {
                'note': post.note,
            }
            self.postord_acc.append(res)
        return self.postord_acc
        
            
    

report_sxw.report_sxw('report.isf.oh.ot.webkit', 'isf.oh.patient.operation', 'addons/isf_open_hospital/report/preot.mako', parser=isf_oh_ot_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: