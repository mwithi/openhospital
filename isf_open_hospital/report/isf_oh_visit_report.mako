<html>
<head>
    <style type="text/css">
        ${css}
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <center>
    <h1><b><i>Visit Module</i></b></h1>
    </center>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print headerlines()
    print lines()
    print chequelines()
    """
    %>

    <table style="width: 100%" border="1">
        %for head in headerlines():
            <tr>
                <td>Patient Id :</td>
                <td>Patient Name</td>
                <td>Sex</td>
                <td>Ward</td>
                <td>Date</td>
            </tr>
            <tr>
                <td>${head['id_number']} </td>
                <td>${head['name']}</td>
                <td>${head['sex']}</td>
                <td>${head['ward']}</td>
                <td>${head['date']}
            </tr>
        %endfor
    </table>

</body>
</html>
