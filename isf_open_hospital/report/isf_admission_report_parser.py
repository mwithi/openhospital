# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import datetime

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug=False

class isf_oh_admission_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.oh.admission.webkit'

    def __init__(self, cr, uid, name, context=None):
        super(isf_oh_admission_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'first_page' : self.first_page,
            'second_page' : self.second_page,
            'history' : self.history,
            'monitoring' : self.monitoring,
        })
        self.context = context
        self.monitor_acc = []
        self.first_acc = []
        self.second_acc = []
        self.history_acc = []
    
    def first_page(self, ids=None, done=None):
        ctx = self.context.copy()
        admission_o = self.pool.get('isf.oh.admission')
        active_id = ctx.get('active_id')
        
        admission = admission_o.browse(self.cr, self.uid, [active_id])[0]
        
        adm_date = admission.admission_date[:10]
        adm_time =  admission.admission_date[10:len(admission.admission_date)]
        res = {
            'card_id' : admission.admission_sequence,
            'name' : admission.patient_id.patient_name,
            'code' : admission.patient_id.name,
            'age' : admission.patient_id.age_yy,
            'ward' : admission.ward.name,
            'bed_no' : admission.bed.number,
            'date' : adm_date,
            'time' : adm_time,
            
        }
        self.first_acc.append(res)
        return self.first_acc
        
    def second_page(self, ids=None, done=None):
        ctx = self.context.copy()
        admission_o = self.pool.get('isf.oh.admission')
        active_id = ctx.get('active_id')
        
        admission = admission_o.browse(self.cr, self.uid, [active_id])[0]
        
        res = {
            'note' : admission.note,
            'treatment' : admission.treatment,
            'weight' : admission.weight,
            'temp' : admission.temperature,
            'bp' : admission.bp,
            'pr' : admission.pr,
            'rr' : admission.rr,
            'diagnosis_in' : admission.diagnosis_in_id.name+" - "+admission.diagnosis_in_id.description,
            
        }
        self.second_acc.append(res)
        return self.second_acc
        
    def history(self, ids=None, done=None):
        ctx = self.context.copy()
        admission_o = self.pool.get('isf.oh.admission')
        diag_o = self.pool.get('isf.oh.disease')
        active_id = ctx.get('active_id')
        
        admission = admission_o.browse(self.cr, self.uid, [active_id])[0]
        patient_id = admission.patient_id.id
        
        admission_ids = admission_o.search(self.cr, self.uid, [('patient_id','=',patient_id),('state','=','discharged')],order='discharge_date desc',limit=5)
        for adm in admission_o.browse(self.cr, self.uid, admission_ids):
            adm_date = adm.admission_date[:10]
            dis_date =  adm.discharge_date[:10]
            adm_code = adm.admission_sequence[4:len(adm.admission_sequence)]
            
            diag_out_list = ''
            for diag in adm.diagnosis_out_ids:
                if _debug:
                    _logger.debug('Diag : %s', diag.description)
                diag_out_list += diag.description + ','
            res = {
                'code' : adm_code.strip(),
                'admit_date' : adm_date,
                'admit_type' : adm.admission_type_ids.name,
                'discharge_date' : dis_date,
                'diagnosis_in' : adm.diagnosis_in_id.description,
                'diagnosis_out' : diag_out_list,
                'discharge_type' : adm.discharge_type_ids.name,
                'bed_no' : adm.bed.number,
                'ward' : adm.ward.name,
            }
            
            self.history_acc.insert(0,res)
        return self.history_acc
            
    
    def monitoring(self, ids=None, done=None):
        ctx = self.context.copy()
        admission_o = self.pool.get('isf.oh.admission')
        active_id = ctx.get('active_id')
        
        admission = admission_o.browse(self.cr, self.uid, [active_id])[0]
        for value in admission.monitoring_ids:
            res = {
                'datetime' : value.datetime,
                'temp' : value.temperature,
                'bp' : value.bp,
                'plus' : value.plus,
                'rr': value.rr,
                'oxygen' : value.oxygen,
                'input' : value.input,
                'output' : value.output,
            }
        
            self.monitor_acc.append(res)
        return self.monitor_acc
        
            
    

report_sxw.report_sxw('report.isf.oh.admission.webkit', 'isf.oh.admission', 'addons/isf_open_hospital/report/admission.mako', parser=isf_oh_admission_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: