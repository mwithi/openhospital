<html>
<head>
    <style type="text/css">
        ${css}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:12px;
}
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print first_page()
    print second_page()
    print monitoring()
    """
    %>

 <div class="break">
    <center>
        <h2>
            ADMISSION CARD
        </h2>
        
        <br/><br/>
        <table style="width:90%" border="0">
        %for data in first_page():
            <tr>
                <td><b>Patient ID :</b></td><td>${data['code']}</td>
                <td><b>Card ID :</b></td><td>${data['card_id']}</td>
            </tr>
            <tr>
                
                <td><b>Name :</b></td><td>${data['name']}</td>
                <td><b>Age :</b></td><td>${data['age']}</td>
            </tr>
            <tr>
                <td><b>Ward :</b></td><td>${data['ward']}</td>
                <td><b>Bed No. :</b></td><td>${data['bed_no']}</td>
            </tr>
            <tr>
                <td><b>Date of Admission :</b></td><td>${data['date']}</td>
                <td><b>Time :</b></td><td>${data['time']}</td>
            </tr>
            <tr>
                <td><b>Admitting Doctor :</b></td>
            </tr>
        %endfor
        </table>
</div>
 <div class="break">       
        <!--
            IPD Patients History, show
            the last 5 admission/discharge
        -->
        <table class="table-data">
        %for data in history():
            <table class="table-data">
                <tr colspan="5" border="0">
                    <td border="0"><b>${data['code']}</b></td>
                    <td border="0"><b>${data['ward']}</b></td>
                </tr>
                <tr>
                    <th class="td-data" style="width: 10%;">Admitted</th>
                    <th class="td-data" style="width: 10%;">Type</th>
                    <th class="td-data" style="width: 10%;">Discharged</th>
                    <th class="td-data" style="width: 10%;">Type</th>
                    <th class="td-data" style="width: 25%;">Diag. In</th>
                    <th class="td-data" style="width: 25%;">Diag. OUT</th>
                </tr>
                <tr>
                    <td class="td-data" style="width: 10%;">${data['admit_date']}</td>
                    <td class="td-data" style="width: 10%;">${data['admit_type']}</td>
                    <td class="td-data" style="width: 10%;">${data['discharge_date']}</td>
                    <td class="td-data" style="width: 10%;">${data['discharge_type']}</td>
                    <td class="td-data" style="width: 25%;">${data['diagnosis_in']}</td>
                    <td class="td-data" style="width: 25%;">${data['diagnosis_out']}</td>
                </tr>
            </table>
        %endfor
        </table> 
    
        <br/><br/>
        %for data in second_page():
            <table class="table-data">
                <caption>Vital Signs</caption>
                <tr>
                    <th class="td-data" style="width: 20%;">Weight</th>
                    <th class="td-data" style="width: 20%;">Temp</th>
                    <th class="td-data" style="width: 20%;">BP</th>
                    <th class="td-data" style="width: 20%;">PR</th>
                    <th class="td-data" style="width: 20%;">RR</th>
                </tr>
            
                <tr>
                    <td class="td-data" style="width: 20%;">${data['weight']} Kg.</td>
                    <td class="td-data" style="width: 20%;">${data['temp']} C</td>
                    <td class="td-data" style="width: 20%;">${data['bp']} mmhg</td>
                    <td class="td-data" style="width: 20%;">${data['pr']} /m</td>
                    <td class="td-data" style="width: 20%;">${data['rr']} /m</td>
                </tr>
            </table>
            <br/>
            <table class="table-data">
                <tr>
                    <th class="td-data">Provisional Diagnosis</th>
                </tr>
            
                <tr>
                    <td class="td-data">${data['diagnosis_in']} </td>
                </tr>
            </table>
            <br/>
            <table class="table-data">
                <tr>
                    <th class="td-data">Treatment on Admission</th>
                </tr>
            
                <tr>
                    <td class="td-data">${data['treatment']} </td>
                </tr>
            </table>
            <br/>
            <table class="table-data">
                <tr>
                    <th class="td-data">Note</th>
                </tr>
            
                <tr>
                    <td class="td-data">${data['note']} </td>
                </tr>
            </table>
        %endfor
        <br/><br/>
    </center>
</div>
 <div class="break">   
     <table class="table-data">
        <caption>Monitoring Sheet</caption>
        %for data in monitoring():
            <tr>
                <th class="td-data" style="width: 20%;">Date/Time</th>
                <th class="td-data" style="width: 20%;">BP</th>
                <th class="td-data" style="width: 20%;">Plus</th>
                <th class="td-data" style="width: 20%;">Temp</th>
                <th class="td-data" style="width: 20%;">RR</th>
                <th class="td-data" style="width: 20%;">Oxygen</th>
                <th class="td-data" style="width: 20%;">Input</th>
                <th class="td-data" style="width: 20%;">Output</th>
            </tr>
            
            <tr>
                <td class="td-data" style="width: 20%;">${data['datetime']}</td>
                <td class="td-data" style="width: 20%;">${data['bp']}</td>
                <td class="td-data" style="width: 20%;">${data['plus']}</td>
                <td class="td-data" style="width: 20%;">${data['temp']}</td>
                <td class="td-data" style="width: 20%;">${data['rr']}</td>
                <td class="td-data" style="width: 20%;">${data['oxygen']}</td>
                <td class="td-data" style="width: 20%;">${data['input']}</td>
                <td class="td-data" style="width: 20%;">${data['output']}</td>
            </tr>
        %endfor
     </table>
 </div>
</body>
</html>
