<html>
<head>
    <style type="text/css">
        ${css}
        
.break{
    display: block;
    clear: both;
    page-break-after: always;
}

.table-data {
    border: 1px solid black;
    border-collapse: collapse;
    width:100%;
}

.table-data-nb {
    border: 0px;
    border-collapse: collapse;
    padding: 30px;
    width:100%;
}

.td-data {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 5px;
    font-family:verdana;
    font-size:12px;
}

.td-data-nb {
    padding: 5px;
    font-family:verdana;
    font-size:12px;
}

#endpage {
    position: relative;
    top: 10px;
    left: 0;
    width: 100%;
}

    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print lines()
    """
    %>


 <div class="break">       
        <h3>PRE-OPERATIVE CHECK LIST AND ANESTHETIC CHART</h3>
        <table class="table-data">
        %for data in lines():
            <table class="table-data">
                <tr>
                    <th class="td-data" style="width: 10%;">Patient</th>
                    <th class="td-data" style="width: 10%;">Age</th>
                    <th class="td-data" style="width: 10%;">Sex</th>
                    <th class="td-data" style="width: 10%;">Ward</th>
                </tr>
                <tr>
                    <td class="td-data" style="width: 10%;">${data['patient_code']}</td>
                    <td class="td-data" style="width: 10%;text-align:right;">${data['patient_age']}</td>
                    <td class="td-data" style="width: 10%;">${data['patient_sex']}</td>
                    <td class="td-data" style="width: 25%;">${data['ward']}</td>
                </tr>
            </table>
            
            <!-- Checklist -->
            <br/>
            <table class="table-data-nb">
                <tr>
                    <td width="33%">
                        <table class="table-data-nb">
                            <caption>Checklist</caption>
                            %if data['ot_informed'] == True:
                                <tr><td>Informed Consent </td><td><input type="checkbox" name="checklist" checked="True"/></td>
                            %endif
                            %if data['ot_informed'] == False:
                                <tr><td>Informed Consent </td><td><input type="checkbox" name="checklist"/></td>
                            %endif
            
                            %if data['ot_shaving'] == True:
                                <tr><td>Shaving </td><td><input type="checkbox" name="checklist" checked="True" /></td>   
                            %endif
                            %if data['ot_shaving'] == False:
                                <tr><td>Shaving </td><td><input type="checkbox" name="checklist"/></td>    
                            %endif
            
                            %if data['ot_catheterizatiomn'] == True:
                                <tr><td>Catetherization </td><td><input type="checkbox" name="checklist" checked="True" /></td>
                            %endif
                             %if data['ot_catheterizatiomn'] == False:
                                <tr><td>Catetherization </td><td><input type="checkbox" name="checklist"  /></td>
                            %endif
            
                            %if data['ot_fasting'] == True:
                                <tr><td>Fasting </td><td><input type="checkbox" name="checklist" checked="True" /></td>
                            %endif
                             %if data['ot_fasting'] == False:
                                <tr><td>Fasting </td><td><input type="checkbox" name="checklist"  /></td>
                            %endif
            
                            %if data['ot_iv_line'] == True:
                                <tr><td>IV Line</td><td><input type="checkbox" name="checklist" checked="True" /></td>
                            %endif
                             %if data['ot_iv_line'] == False:
                                <tr><td>IV Line </td><td><input type="checkbox" name="checklist" /></td>
                            %endif
                        </table>
                    </td>
                    <td width="33%" valign="top">
                        <table class="table-data-nb">
                            <caption>Allergy</caption>
                            <tr><td class="td-data-nb">${data['allergy']}</td>
                        </table>       
                    <td>
                    <td width="33%" valign="top">
                        <table class="table-data-nb">
                            <caption>Anesthetic Risk</caption>
                            <tr><td class="td-data-nb">${data['anesthetic_risk']}</td>
                        </table>
                    </td>
                <tr>
            </table>
            
            <br/>
            <h3>INTRA OPERATIVE</h3>
            <!-- Intra Operative -->
            <table class="table-data">
                <tr>
                    <th class="td-data" style="width: 10%;">Anesthesist</th>
                    <th class="td-data" style="width: 10%;">Operating Doctor</th>
                    <th class="td-data" style="width: 10%;">Method</th>
                </tr>
                <tr>
                    <td class="td-data" style="width: 10%;">${data['anesthesist']}</td>
                    <td class="td-data" style="width: 10%;">${data['doctor']}</td>
                    <td class="td-data" style="width: 10%;">${data['method']}</td>
                </tr>
            </table>
        %endfor
        </table> 
        
        <!-- Operation Data-->
        <br/>
        <table class="table-data-nb">
            <tr>
                <td width="33%" valign="top">
                    <table class="table-data-nb">
                        <caption>Induction Time</caption>
                        <tr>
                            <th/>
                            <th>Time</th>
                            <th>Note</th>
                        </tr>
                        %for data in inductions():
                        <tr>
                            <td class="td-data-nb">${data['index']})</td>
                            <td class="td-data-nb">${data['time']}</td>
                            <td class="td-data-nb">${data['note']}</td>
                        </tr>
                        %endfor
                    </table>
                </td>
                <td width="33%" valign="top">
                    <table class="table-data-nb">
                        <caption>Maintenance</caption>
                        <tr>
                            <th/>
                            <th>Time</th>
                            <th>Note</th>
                        </tr>
                        %for data in maintenance():
                        <tr>
                            <td class="td-data-nb">${data['index']})</td>
                            <td class="td-data-nb">${data['time']}</td>
                            <td class="td-data-nb">${data['note']}</td>
                        </tr>
                        %endfor
                    </table>
                </td>
                <td width="33%" valign="top">
                    <table class="table-data-nb">
                        <caption>Intra-operative Drugs</caption>
                        <tr>
                            <th/>
                            <th>Drugs</th>
                            <th>Qty</th>
                        </tr>
                        %for data in intraoperative():
                        <tr>
                            <td class="td-data-nb">${data['index']})</td>
                            <td class="td-data-nb">${data['drug']}</td>
                            <td class="td-data-nb" style="text-align:right;">${data['qty']}</td>
                        </tr>
                        %endfor
                    </table>
                </td>
            </tr>   
        </table>
        
        <!-- Observations-->
        <br/>
        <table class="table-data">
            <tr>
            %for data in observations():
                <td class="td-data"><b>Time</b></td>
                %for dt in data['time_ids']:
                    <td class="td-data" style="text-align:right;">${dt}</td>
                %endfor
                <tr/>
                <td class="td-data"><b>BP (mmhg)</b></td>
                %for dbp in data['bp_ids']:
                    <td class="td-data" style="text-align:right;">${dbp}</td>
                %endfor
                <tr/>
                <td class="td-data"><b>Pulse/min</b></td>
                %for dp in data['pulse_ids']:
                    <td class="td-data" style="text-align:right;">${dp}</td>
                %endfor
            %endfor
        </table>
        
        <!-- Footer -->
       <!--<div id="endpage"> -->
            <br/><br/>
            <table class="table-data-nb">
                %for data in footerdata():
                <tr>
                    <td class="td-data-nb"><b>End Of Surgery</b></td>
                    <td class="td-data-nb">${data['endos']}</td>
                    <td class="td-data-nb"><b>EBL (Estimated Blood Loss)</b></td>
                    <td class="td-data-nb" style="text-align:right;">${data['ebl']}</td>
                </tr>
                <tr>
                    <td class="td-data-nb"><b>Patient Condition</b></td>
                    <td class="td-data-nb">${data['codition']}</td>
                </tr>
                <tr>
                    <td class="td-data-nb"><b>Special Instructions</b></td>
                    <td class="td-data-nb" colspan="3">${data['instructions']}</td>
                </tr>
                %endfor
            </table>
            <br></br>
            <table class="table-data-nb">
                <tr>
                    <td class="td-data-nb">Signature .......................................</td>
                    <td class="td-data-nb">Date ......................</td>
                <tr>
            </table>
        <!--</div>-->
 </div>
 <!--
 ####################################################################################################################################
 -->
        <div class="break">
            <h3>SURGERY NOTES</h3>
            <!-- Data Header -->
            %for data in lines():
            <table class="table-data">
                <tr>
                    <th class="td-data" style="width: 25%;">Admission Code</th>
                    <th class="td-data" style="width: 25%;">Admission Date</th>
                    <th class="td-data" style="width: 25%;">Ward</th>
                    <th class="td-data" style="width: 25%">Bed No.</th>
                </tr>
                <tr>
                    <td class="td-data" style="width: 25%;">${data['admission_code']}</td>
                    <td class="td-data" style="width: 25%;">${data['date_of_admission']}</td>
                    <td class="td-data" style="width: 25%;">${data['ward']}</td>
                    <td class="td-data" style="width: 25%;text-align:right;">${data['bed_no']}</td>
                </tr>
            </table>
            <!-- Patient Header -->
            <table class="table-data">
                <tr>
                    <th class="td-data" style="width: 33%;">Patient</th>
                    <th class="td-data" style="width: 33%;">Age</th>
                    <th class="td-data" style="width: 33%;">Sex</th>
                </tr>
                <tr>
                    <td class="td-data" style="width: 33%;">${data['patient_code']}</td>
                    <td class="td-data" style="width: 33%;text-align:right;">${data['patient_age']}</td>
                    <td class="td-data" style="width: 33%;">${data['patient_sex']}</td>
                </tr>
            </table>
            %endfor
            
            %for data in surgerynote():
            <table class="table-data">
                 <tr>
                    <th class="td-data" style="width: 50%;">Pre Operative Diagnosis</th>
                    <th class="td-data" style="width: 50%;">Post Operative Diagnosis</th>
                 </tr>
                 <tr>   
                    <td class="td-data" style="width: 50%;">${data['prediag']}</td>
                    <td class="td-data" style="width: 50%;">${data['postdiag']}</td>
                 </tr>
            </table>
            <table class="table-data">
                 <tr>
                    <th class="td-data" style="width: 33%;">Surgeon</th>
                    <th class="td-data" style="width: 33%;">Assistant</th>
                    <th class="td-data" style="width: 33%;">Anesthesist</th>
                 </tr>
                 <tr>   
                    <td class="td-data" style="width: 33%;">${data['doctor']}</td>
                    <td class="td-data" style="width: 33%;">${data['assistant']}</td>
                    <td class="td-data" style="width: 33%;">${data['anesthesist']}</td>
                 </tr>
            </table>
            <br/>
            <table class="table-data">
                <tr>
                    <th class="td-data">Indication For Surgical Procedure</th>
                </tr>
                %for note in data['surg_note_ids']:
                <tr>
                    <td class="td-data">${note}</td>
                </tr>
                %endfor
            </table>
            %endfor
            <br/>
            
            <table class="table-data">
                <tr>
                    <th class="td-data">Procedure Report</th>
                </tr>
                %for data in procedurereport():
                <tr>
                    <td class="td-data">${data['note']}</td>
                </tr>
                %endfor
            </table>
            <br/>
            
            <table class="table-data">
                <tr>
                    <th class="td-data">Post Operative Orders</th>
                </tr>
                %for data in postord():
                <tr>
                    <td class="td-data">${data['note']}</td>
                </tr>
                %endfor
            </table>
            <br/>
        </div>
</body>
</html>
