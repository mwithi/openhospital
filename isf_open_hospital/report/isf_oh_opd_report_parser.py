# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import datetime

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug=False

class isf_oh_opd_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.oh.opd.webkit'
    
     
    def __init__(self, cr, uid, name, context=None):
        super(isf_oh_opd_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'first_page' : self.first_page,
        })
        self.context = context
        self.first_acc = []
    
    def first_page(self, ids=None, done=None):
        ctx = self.context.copy()
        opd_o = self.pool.get('isf.oh.opd')
        active_id = ctx.get('active_id')
        
        opd = opd_o.browse(self.cr, self.uid, [active_id])[0]
        
        attendance_date = opd.attendance_date
        
        exams = []
        for exam in opd.exam_ids:
            for request in exam.exams_id:
                exams.append(request.exam_id.name)
            
        #imagings = []
        #for image in opd.imaging_ids:
        #    imagings.append(image.name)
            
        others = []
        for other in opd.other_ids:
            others.append(other.name)
            
        diagnosis = []
        for diag in opd.diagnosis_ids:
            diagnosis.append(diag.name+" - "+diag.description)
            
        drugs = []
        for line in opd.drugs_ids:
            rec = {'drug': line.drug_id.name, 'qty' : line.qty}
            drugs.append(rec)
        
        for other_drug in opd.other_drugs_ids:
            rec = {'drug': other_drug.text, 'qty' : other_drug.qty}
            drugs.append(rec)
            
        orders = []
        for order in opd.order_ids:
            orders.append(order.name)
    
        action_taken_desc = 'Follow Up (In Days)'
        if opd.action_taken == 'ht':
            action_taken_desc = 'Home Treatment'
        elif opd.action_taken == 'ra':
            action_taken_desc = 'Referred/Admitted'
            
        res = {
            'date' : attendance_date,
            'card_id' : opd.opd_sequence,
            'name' : opd.patient_name,
            'code' : opd.patient_id.partner_id.name,
            'age' : opd.patient_yy,
            'sex' : opd.patient_sex,
            'date' : attendance_date,
            'present_compliant' : opd.present_compliant,
            'observation' : opd.observation,
            'summary' : opd.summary,
            # Investigation
            'exams' : exams,
            #'imagings' : imagings,
            'others' : others,
             # Disgnosis
             'disease' : '',
             'diagnosis' : '',
             'action_taken' : opd.action_taken,
             'action_taken_desc' : action_taken_desc,
             'follow_up_days' : opd.follow_up_days,
             'orders' : orders,
             'drugs' : drugs,
             'doctor' : opd.doctor_id.name,
            
        }
        self.first_acc.append(res)
        return self.first_acc
        
    
report_sxw.report_sxw('report.isf.oh.opd.webkit', 'isf.oh.opd', 'addons/isf_open_hospital/report/opd.mako', parser=isf_oh_opd_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: