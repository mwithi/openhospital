# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale

try:
    locale.setlocale(locale.LC_ALL,'')
except:
    pass

_logger = logging.getLogger(__name__)
_debug=True

class isf_oh_patient_number_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.oh.patient.number.webkit'

    def __init__(self, cr, uid, name, context=None):
        super(isf_oh_patient_number_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'header' : self.header,
        })
        self.context = context
        self.result_acc = []
        self.header_acc = []

    def _statistics_group_by_day(self, cr, uid, ids, date_start, date_stop):
#         cr.execute("""SELECT t1.date, t1.male, t2.female
#             from (select p1.registration_date as date, count(p1.sex) as male
#             from isf_oh_patient p1
#             where p1.sex = 'm'
#             and p1.registration_date >= '%s' and p1.registration_date <= '%s' 
#             group by 1,p1.sex
#             order by 1 asc ) t1
#             left join
#             (select 
#                 p2.registration_date as date, count(p2.sex) as female
#                 from isf_oh_patient p2
#             where p2.sex = 'f'
#             and p2.registration_date >= '%s' and p2.registration_date <= '%s' 
#             group by 1,p2.sex
#             order by 1 asc) t2
#             on (t1.date = t2.date)""" % (date_start,date_stop,date_start,date_stop))
        
        cr.execute("""SELECT DATE(p1.registration_date), 
            COUNT(CASE WHEN p1.sex = 'm' THEN 1 END) as male, 
            COUNT(CASE WHEN p1.sex = 'f' THEN 1 END) as female
            FROM isf_oh_patient p1
            WHERE DATE(p1.registration_date) >= '%s' AND DATE(p1.registration_date) <= '%s'
            GROUP BY DATE(p1.registration_date)""" % (date_start,date_stop))

        result = cr.dictfetchall()
        total = 0
        total_male = 0
        total_female = 0
        for res in result:
            total_male += res['male'] or 0
            total_female += res['female'] or 0
            total += ((res['male'] or 0) + (res['female'] or 0))
            d = {
                'is_total' : '0',
                'date' : res['date'],
                'male' : res['male'] or 0,
                'female' : res['female'] or 0,
                'total' : (res['male'] or 0)+ (res['female'] or 0),
            }
            self.result_acc.append(d)
        d = {
            'is_total' : '1',
            'date' : False,
            'male' : total_male,
            'female' : total_female,
            'total' : total,
        }
        self.result_acc.append(d)

        return self.result_acc

    def _statistics_group_by_month(self, cr, uid, ids, date_start, date_stop):
        cr.execute("""SELECT t1.mon, t1.male, t2.female
            from (select to_char(p1.registration_date,'Mon') as Mon, count(p1.sex) as male
            from isf_oh_patient p1
            where p1.sex = 'm'
            and p1.registration_date >= '%s' and p1.registration_date <= '%s' 
            group by 1,p1.sex
            order by 1 asc ) t1
            left join
            (select 
                to_char(p2.registration_date,'Mon') as Mon, count(p2.sex) as female
                from isf_oh_patient p2
            where p2.sex = 'f'
            and p2.registration_date >= '%s' and p2.registration_date <= '%s' 
            group by 1,p2.sex
            order by 1 asc) t2
            on (t1.mon = t2.mon)""" % (date_start,date_stop,date_start, date_stop))

        result = cr.dictfetchall()
        total = 0
        total_male = 0
        total_female = 0
        for res in result:
            total_male += res['male'] or 0
            total_female += res['female'] or 0
            total += (res['male'] + res['female'])
            d = {
                'is_total' : '0',
                'date' : res['mon'],
                'male' : res['male'] or 0,
                'female' : res['female'] or 0,
                'total' : res['male'] + res['female'],
            }
            self.result_acc.append(d)
        d = {
            'is_total' : '1',
            'date' : False,
            'male' : total_male,
            'female' : total_female,
            'total' : total,
        }
        self.result_acc.append(d)

        return self.result_acc
    
    def header(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.oh.patient.number.wizard')
        report_date_stop = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_stop'])
        report_date_start = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_start'])
        report_group_type = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['group_type'])
        date_start = report_date_start[0]['date_start']
        date_stop = report_date_stop[0]['date_stop']
        group_type = report_group_type[0]['group_type']
        
        if _debug:
            _logger.debug('date_start : %s', date_start)
            _logger.debug('date_stop : %s', date_stop)
            _logger.debug('group_type : %s', group_type)
        
        res = {
            'date_start' : date_start,
            'date_stop' : date_stop,
            'group_type' : group_type,
        }

        self.header_acc.append(res)
        return self.header_acc

    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.oh.patient.number.wizard')
        report_date_stop = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_stop'])
        report_date_start = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_start'])
        report_group_type = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['group_type'])
        
        date_start = report_date_start[0]['date_start']
        date_stop = report_date_stop[0]['date_stop']
        group_type = report_group_type[0]['group_type']

        cr = self.cr
        uid = self.uid

        if group_type == 'day':
            return self._statistics_group_by_day(cr, uid, ids, date_start,date_stop)
        elif group_type == 'month':
            return self._statistics_group_by_month(cr, uid, ids, date_start,date_stop)

report_sxw.report_sxw('report.isf.oh.patient.number.webkit', 'isf.oh.patient', 'addons/isf_open_hospital/report/isf_oh_patient_number_report.mako', parser=isf_oh_patient_number_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
