# -*- coding: utf-8 -*-

{
	'name': 'ISF HIMS Finger Print Support',
	'version': '0.4.0',
	'category': 'Tools',
	'description': """

ISF HIMS Finger Print Support
=============================

* Digital Persona Support U.ARE.U 4500
* Finger Print Save on Patient Browser
* Finger Print Search on Search Patient Function
* Finger Print Search on CIMS PoS service

""",
	'author': 'Matteo Angelino (matteo.angelino@informaticisenzafrontiere.org)',
	'website': 'www.informaticisenzafrontiere.org',
	'license': 'AGPL-3',
	'depends': [
        'isf_open_hospital','isf_cims_module'
    ],
	'data': [   
        'security/oh_fp_security.xml',
        'views/isf_oh_patient_view.xml',
        'views/isf_pos_finger_print_view.xml'
        #'security/ir.model.access.csv',
        #'data/csv/isf.home.dashboard.action.csv',
    ],
	'css' : [],
	'demo': [],
	'installable' : True,
}

